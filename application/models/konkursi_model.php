<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Konkursi_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function save_application() {
        //echo date('Y-m-d',strtotime($this->input->post("datum_rodjenja"))); die();
        $ostalo = Array(
                'univerzitet_sarajevo' => $this->input->post("univerzitet_sarajevo"),
                'redovan' => $this->input->post("redovan"),
                'uvjerenje_redovno_skolovanje' => $this->input->post("uvjerenje_redovno_skolovanje"),
                'uvjerenje_polozeni_ispiti' => $this->input->post("uvjerenje_polozeni_ispiti"),
                'prima_stipendiju' => $this->input->post("prima_stipendiju"),
                'izjava_o_neprimanju_stipendije' => $this->input->post("izjava_o_neprimanju_stipendije"),
                'stalno_prebivaliste' => $this->input->post("stalno_prebivaliste"),
                'fotografije' => $this->input->post("fotografije"),
                'samo_jedna_prijava' => $this->input->post("samo_jedna_prijava"),
                'uvjerenje_domacinstvo' => $this->input->post("uvjerenje_domacinstvo"),
                'dokaz_o_primanjima' => $this->input->post("dokaz_o_primanjima"),
                'dokaz_o_rvi' => $this->input->post("dokaz_o_rvi"),
        );
        
        $bodovi = $this->get_bodovi($this->input->post("trajanje_studija"), $this->input->post("godina_studija"), $this->input->post("prosjek_ocjena"), $this->input->post("ukupna_primanja"), $this->input->post("ukupno_clanova"), $this->input->post("roditelji"), $this->input->post("porodica_rvi"));
        
        $application = Array (
                'konkurs_id' => 'konkurs_id',
                'ime' => ucwords($this->input->post("ime")),
                'prezime' => ucwords($this->input->post("prezime")),
                'ime_roditelja' => ucwords($this->input->post("ime_roditelja")),
                'telefon' => $this->input->post("telefon"),
                'mobitel' => $this->input->post("mobitel"),
                'email' => $this->input->post("email"),
                'adresa' => $this->input->post("adresa"),
                'opstina' => $this->input->post("opstina"),
                'jmbg' => $this->input->post("jmbg"),
                'broj_licne_karte' => strtoupper($this->input->post("broj_licne_karte")),
                'datum_rodjenja' => date('Y-m-d',strtotime($this->input->post("datum_rodjenja"))),
                'mjesto_rodjenja' => ucwords($this->input->post("mjesto_rodjenja")),
                'gender' => $this->input->post("gender"),
                'fakultet' => $this->input->post("fakultet"),
                'trajanje_studija' => $this->input->post("trajanje_studija"),
                'godina_studija' => $this->input->post("godina_studija"),
                'prosjek_ocjena' => $this->input->post("prosjek_ocjena"),
                'ukupna_primanja' => $this->input->post("ukupna_primanja"),
                'ukupno_clanova' => $this->input->post("ukupno_clanova"),
                'roditelji' => $this->input->post("roditelji"),
                'porodica_rvi' => $this->input->post("porodica_rvi"),
                'komentar' => $this->input->post("komentar"),
                'ostalo' => serialize($ostalo),
                'status' => '0', // status aplikacije! 0 => poslano ; 1 => email poslan; 2 => email potvrda ; 3 => sms poslan ; 4 => sms primljen(ovo ima u status o SMS-u) ; 5 => sms potvrdjen
                'sms_kod' => strtoupper(random_string('alnum', 6)),
                'sms_status' => '000',
                'ip' => $this->input->ip_address(),
                'datetime' => date('Y-m-d H:i:s',time()),
                'session' => serialize($this->session->all_userdata()),
                'session_id' => md5($this->session->userdata('session_id')),
                'bodovi' => $bodovi['ukupno'],
                'bodovanje' => serialize($bodovi)
                );
                                    
                $this->db->insert('konkursi', $application);
            
                return true;
    }
    
    function get_bodovi($trajanje_studija, $godina_studija, $prosjek_ocjena, $ukupna_primanja, $ukupno_clanova, $roditelji, $porodica_rvi) {
        $skolovanje = 0;
        $materijalno_stanje = 0;
        $status_uzih_clanova_porodice = 0;

        # 1) ŠKOLOVANJE ( 35 poena)
            # a) Prosjek ocjena - (25 poena)
               # za brucose BROJ POENA=25+(20/3)(PROSJEK-5)
               # za starije godine BROJ POENA=25+5(PROSJEK-10)
        
            if ($godina_studija == 1) {
                $prosjek_ocjena = 25 + (20 / 3) * ($prosjek_ocjena-5);
            } elseif ($godina_studija > 1) {
                $prosjek_ocjena = 25 + 5 * ($prosjek_ocjena-10);
            }

            # b) GODINA STUDIJA ( 10 poena )
                #    - Za studij od šest godina: 1,67 poena po godini;
                #    - Za studij od pet godina: 2,0 poena po godini;
                #    - Za studij od četiri godine: 2,5 poena po godini;
                #    - Za studij od tri godine: 3,33 poena po godini;
            
            switch ($trajanje_studija) {
                case '6': $godina_studija = 1.67 * $godina_studija; break;
                case '5': $godina_studija = 2 * $godina_studija; break;
                case '4': $godina_studija = 2.5 * $godina_studija; break;
                case '3': $godina_studija = 3.33 * $godina_studija; break;
            }
            
            $skolovanje = $prosjek_ocjena + $godina_studija;
            
        # 2) MATERIJALNO STANJE ( 55 poena )
                # Prosječna primanja po članu porodice
                # 0,00 - 40,00 KM (55 poena)
                # 41,00 - 70,00 KM (45 poena)
                # 71,00 - 100,00 KM (30 poena)
                # 101,00 - 150,00 KM (20 poena)
                # 151,00 - 200,00 KM (10 poena)
                # IZNAD 200,00 KM (5 poena)
//            if ($ukupna_primanja == 0) $ukupna_primanja = 1; // vea add - stupid fix
//            $tmp = $ukupna_primanja / $ukupno_clanova;
//            switch ($tmp) {
//                case ($tmp >= 0 && $tmp <=40): $materijalno_stanje = 55; break;
//                case ($tmp >= 41 && $tmp <= 70): $materijalno_stanje = 45; break;
//                case ($tmp >= 71 && $tmp <= 100): $materijalno_stanje = 30; break;
//                case ($tmp >= 101 && $tmp <= 150): $materijalno_stanje = 20; break;
//                case ($tmp >= 151 && $tmp <= 200): $materijalno_stanje = 10; break;
//                case ($tmp >= 201): $materijalno_stanje = 5; break;
//            }
            
             $PrimanjaPoClanu = $ukupna_primanja / $ukupno_clanova;
             if ($PrimanjaPoClanu >= 201) {
             $materijalno_stanje = 5;
             } else {
             $materijalno_stanje = 55 - (($PrimanjaPoClanu*50)/200);
            }
             
        # 3) STATUS UŽIH ČLANOVA PORODICE ( 10 poena )
            # a)  Student bez jednog roditelja = 4 poena;
            # b)  Student bez oba roditelja = 8 poena;
            # c)  Student RVI, civilne žrtve rata = 2 poena.
        
            # $roditelji = Array('1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
            
            switch ($roditelji) {
                case 1: $roditelji_bodova = 0; break;
                case 2: $roditelji_bodova = 4; break;
                case 3: $roditelji_bodova = 8; break;
            }
            
            if ($porodica_rvi == 1) $porodica_rvi_bodova = 2; else $porodica_rvi_bodova = 0;
            
            $status_uzih_clanova_porodice = $roditelji_bodova + $porodica_rvi_bodova;
            
            $bodovi = array(
                'prosjek_ocjena' => $prosjek_ocjena,
                'godina_studija' => $godina_studija,
                'ukupno_skolovanje' => $skolovanje,
                'materijalno_stanje' => $materijalno_stanje,
                'roditelji_bodova' => $roditelji_bodova,
                'porodica_rvi_bodova' => $porodica_rvi_bodova,
                'status_uzih_clanova_porodice' => $status_uzih_clanova_porodice,
                'ukupno' => $skolovanje + $materijalno_stanje + $status_uzih_clanova_porodice,
            );
            
        return $bodovi;
    }

}