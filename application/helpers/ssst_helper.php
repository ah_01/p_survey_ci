<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Returns list of languages in and array - based on dirs created in application/language/ directory
 * OR If there is no dirs there, it sets the language from config.
 */

if ( ! function_exists('get_languages'))
{
    function get_languages($empty = FALSE)
    {
    	$path = getcwd();
    	$list = scandir($path.'/application/language');
    	unset($list[0]);
    	unset($list[1]);

    	if (empty($lsit)) {
	    	if($empty) $list[1] = "";
		    	foreach ($list as $lang) {
		    		$languages[$lang] = $lang;
	    		}
    	} else {
    		$CI =& get_instance();
    		if($empty) $languages[1] = "";
    		$languages = array($CI->config->item('language'));//$this->config->item('language');
    	}

    	return $languages;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! function_exists('set_language'))
{
    function set_language($language)
    {
    	//$this->session->set_userdata('language',$language);
    	echo $language;
    	return true;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! function_exists('get_array_subnode'))
{
    function get_array_subnode($array = array(), $filter = '')
    {
            foreach ($array as $key => $value) {
                    $result[$key] = $value[$filter];
            }

            return $result;
    }
}


// ------------------------------------------------------------------------

/**
 * Random Element - Takes an array as input and returns a random element
 *
 * @access	public
 * @param	array
 * @param	string
 * @return	mixed	depends on what the array contains
 */
if ( ! function_exists('subnode'))
{
	function subnode($array, $subnode)
	{
                                    $return = array();

                                    if ( ! is_array($return))
                                    {

                                    }

                                    foreach ($array as $key => $value) {
                                            $result[$key] = $value[$subnode];
                                    }
                                    return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * Elements
 *
 * Returns only the array items specified.  Will return a default value if
 * it is not set.
 *
 * @access	public
 * @param	array
 * @param	array
 * @param	mixed
 * @return	mixed	depends on what the array contains
 */
if ( ! function_exists('subnodes'))
{
	function subnodes($items, $array, $default = FALSE)
	{
		$return = array();

		if ( ! is_array($items))
		{
			$items = array($items);
		}

		foreach ($items as $item)
		{
			if (isset($array[$item]))
			{
				$return[$item] = $array[$item];
			}
			else
			{
				$return[$item] = $default;
			}
		}

		return $return;
	}
}

