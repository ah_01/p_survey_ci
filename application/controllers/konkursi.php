<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *  klasa za konkurse!
 * FUNKCIJA 1: ispis formulara
 * FUNKCIJA 2: provjera unesenih podataka
 * FUNKCIJA 3: validacija emaila
 * FUNKCIJA 4: validacija sms-a
 * FUNKCIJA 5: ispis tabele rezultata (iz adminija!!!)
 * FUNKCIJA 6: ispis pojedinacnih podataka za svakog aplikanta (iz adminija!!!)
 * 
 *  vea add - OVO EJ NAJGORI KOD KOJI SAM NAPISAO ZA ZIVOTA!!! NAJGORIIII!!!
 * 
 */

//        $this->output->enable_profiler(TRUE);

class Konkursi extends CI_Controller {
    
    function __construct() {
        parent::__construct();
            date_default_timezone_set('Europe/Sarajevo');
            $this->_is_active();
            $this->load->model('Konkursi_model', 'konkurs');
    }
    
    public function index() {
        $this->load->helper('captcha_functions');
        $data['captcha'] = create_captcha_data();

            if ($this->uri->segment(2) == 'submit_konkurs') {
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<span style="color: #990000;"><img src="/images/icons/error.png" class="icon3" />','<br /></span>');

                if ($this->form_validation->run('konkurs_form_validate') == TRUE) {
                        $session_exists = $this->db->where('session_id',md5($this->session->userdata('session_id')))->where('status !=','-1')->get('konkursi')->num_rows();
                        $ip = $this->db->where('ip',$this->input->ip_address())->where('status !=','-1')->get('konkursi')->num_rows();

                        if ($session_exists != 0 && $ip != 0) { // dovoljno je da je ista sesija i da je sa iste IP adrese. Neka se vremenski oslanja da duzinu trajanja sesije!!!
                            $data['stage'] = 'error';
                            $data['message'] = 'Prijavu je moguće izvršiti samo jednom!';
                        } else {
                            $this->konkurs->save_application();
                            $this->send_email();
                            $data['stage'] = 'email';
                            $data['message'] = 'Na email adresu koju ste unjeli ['.$this->input->post("email").'] će uskoro doći poruka u kojoj će biti link za potvrdu ove prijave. U slučaju da poruka ne stigne u roku par minuta, provjeri te i vaše spam poruke.';
                        }
                            $this->load->view('konkursi/validate',$data);
                } else {
                    $this->load->view('konkursi/konkurs-2012-2013',$data);
                }
            } elseif ($this->uri->segment(2) == 'off') {
                    $data['stage'] = 'off';
                    $data['message'] = 'KONKURS JE ZATVOREN';
                    $data['message'] .= '<br />Rezultate konkursa pratite na web portalu o stipendijama <a href="http://www.stipendije.ba/" target="_blank">www.stipendije.ba</a>';
                    $this->load->view('konkursi/validate',$data);
            } else {
                    $this->load->view('konkursi/konkurs-2012-2013',$data);
            }
    }

    public function preview() {
        if ($this->uri->segment(3) == 'sms_validate') {
            $data['stage'] = 'email_validate_accept';
            // $data['mobitel'] = $this->db->where('session_id',$session_id)->get('konkursi')->row()->mobitel;
            $data['mobitel'] = '38761123456';
            $data['message'] = 'Na broj mobitela koji ste naveli u aplikaciji - <strong>+'.$data['mobitel'].'</strong> - poslan je kod za aktivaciju.<br />';
            $this->load->view('konkursi/validate',$data);
        } else {
            $this->load->helper('captcha_functions');
            $data['captcha'] = create_captcha_data();
            $data['default'] = '';
            $this->load->view('konkursi/konkurs-2012-2013',$data);
        }
    }

    public function email_validate() {
        $action = $this->uri->segment(3);
        $session_id = $this->uri->segment(4);
        if ($session_id != FALSE) {
            if ($this->db->where('session_id',$session_id)->get('konkursi')->num_rows()) {
                $status = $this->db->where('session_id',$session_id)->get('konkursi')->row()->status;
            } else {
                $action = 'error';
            }
        } else {
                $action = ''; // vea add - DIRTY HACK HORRIBLE CODE!!!
                $data['stage'] = 'error';
                $data['message'] = 'Došlo je do neke greške! 004';
        }
        
        switch ($action) {
            case 'accept':
                    if ($status <= 3 && $session_id != FALSE) {
                            $data['stage'] = 'email_validate_accept';
                            $data['mobitel'] = $this->db->where('session_id',$session_id)->get('konkursi')->row()->mobitel;
                            $data['message'] = 'Na broj mobitela koji ste naveli u aplikaciji - <strong>+'.$data['mobitel'].'</strong> - poslan je kod za aktivaciju.<br />';
                            $sms_kod = $this->db->where('session_id',$session_id)->get('konkursi')->row()->sms_kod;
                            $id = $this->db->where('session_id',$session_id)->get('konkursi')->row()->id;
                                if ($status == 1) {
                                    $this->db->set('status',2)->where('session_id',$session_id)->update('konkursi');
                                    if ($status <= 2) {
                                        $sms = $this->send_sms($data['mobitel'],$sms_kod,$id);
                                        if ($sms == TRUE) $this->db->set('status',3)->where('session_id',$session_id)->update('konkursi');
                                    }
                                }
                        } elseif ($status == -1) {
                            $data['stage'] = 'error';
                            $data['message'] = 'Vaša prijava je već poništena!';
                        } elseif ($status == 4) {
                                $data['stage'] = 'info';
                                $data['message'] = 'Ova aplikacija je već aktivirana! Hvala!';
                        }
                break;
            case 'deny':
                    if ($status <= 3) {
                        $this->db->set('status','-1')->where('session_id',$session_id)->update('konkursi');
                        $data['stage'] = 'info';
                        $data['message'] = 'Vaša prijava je poništena!';
                    } else {
                        $data['stage'] = 'error';
                        $data['message'] = 'Ova aplikacija je već aktivirana i nije je moguće poništiti!';
                    }
                break;
            default:
                    $data['stage'] = 'error';
                    $data['message'] = 'Došlo je do neke greške! 004';
                break;
        }
        
        $this->load->view('konkursi/validate',$data);
    }
    
    public function sms_validate() {
        // vea add - OVO EJ NAJGORI KOD KOJI SAM NAPISAO ZA ZIVOTA!!!
        $session_id = $this->input->post('session_id');
        $status = $this->db->where('session_id',$session_id)->get('konkursi')->row()->status;

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span style="color: #990000;"><img src="/images/icons/error.png" class="icon3" />','<br /></span>');
            
        if ($this->form_validation->run('sms_validate') == TRUE) {
            $status = $this->db->where('session_id',$session_id)->get('konkursi')->row()->status;
            if ($status >= 4) {
                $data['stage'] = 'info';
                $data['message'] = 'Vaša aplikacija je potvrđena. Hvala!';
                $data['thankyou'] = true;
            } elseif ($status == 3) {
                $data['stage'] = 'email_validate_accept';
                $data['mobitel'] = $this->db->where('session_id',$session_id)->get('konkursi')->row()->mobitel;
                $data['message'] = 'Na broj mobitela koji ste naveli u aplikaciji - <strong>+'.$data['mobitel'].'</strong> - poslan je kod za aktivaciju.<br />';
            } 
            
        } else {
                $data['stage'] = 'email_validate_accept';
                $data['mobitel'] = $this->db->where('session_id',$session_id)->get('konkursi')->row()->mobitel;
                $data['message'] = 'Na broj mobitela koji ste naveli u aplikaciji - <strong>+'.$data['mobitel'].'</strong> - poslan je kod za aktivaciju.<br />';
        }
        $this->load->view('konkursi/validate',$data);
    }
    
    public function topdf() {
        $data['aplikacija'] = $this->db->where('status !=','-1')->where('session_id',$this->uri->segment(3))->get('konkursi')->row_array();
        $this->load->library('mpdf');
        $this->mpdf->WriteHTML($this->load->view('konkursi/print-aplikacija',$data,true));
        $this->mpdf->SetTitle('Konkurs "Udruženje Spajalica" 2012 - 2013 - '.$data['aplikacija']['ime'].' '.$data['aplikacija']['prezime']);
        $this->mpdf->SetHTMLFooter('<hr  style="height: 1px;"/>© Copyright <a href="http://www.spaja-lica.com" style="text-decoration: none; " >Udruženje "Spajalica"</a>, 2012.');
        $title = 'Udruženje Spajalica - '.$data['aplikacija']['ime'].' '.$data['aplikacija']['prezime'];
        $this->mpdf->Output($title.'.pdf','I');
        exit;
    }

    /* PRIVATE FUNCTIONS!!! */
    
    private function send_email() {
//                $opstine = $this->config->item('opstine');
//                $fakulteti = $this->config->item('fakulteti');
//                $roditelji = Array('1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
//                $trajanje_studija = Array('3'=>'3 godine', '4'=>'4 godine','5'=>'5 godina','6'=>'6 godina');
//                $godina_studija = Array('1'=>'1. godina','2'=>'2. godina','3'=>'3. godina','4'=>'4. godina','5'=>'5. godina','6'=>'6. godina');
//                $neda = Array('0'=>'NE','1'=>'DA');
        
                $name = $this->input->post("ime").' '.$this->input->post("prezime");
                $email = $this->input->post("email");
                $user_ip = $this->input->ip_address();
                $user_agent = $this->input->user_agent();
                $session_id = md5($this->session->userdata('session_id'));
                
                $accept_url = 'http://www.spaja-lica.com/konkursi/email_validate/accept/'.$session_id;
                $deny_url = 'http://www.spaja-lica.com/konkursi/email_validate/deny/'.$session_id;
                $form_url = 'http://www.spaja-lica.com/index.php/konkursi/topdf/'.$session_id;
                
                $message = "----------------------------------------------------------------------------------------------------------------------<br />";
                $message .= "<strong>Uspješno ste obavili prvi korak.</strong><br />";
                $message .= "----------------------------------------------------------------------------------------------------------------------<br /><br />";
                $message .= "Kako biste potvrdili vašu prijavu, kliknite na <strong>link za potvrdu</strong> ispod:<br /><br /><a href=\"$accept_url\">$accept_url</a><br /><br />";
                $message .= "Ukoliko se ne slažete sa navedenim podacima ili je pak ova prijava izvršena bez Vašeg znanja, molimo da kliknete na:<br /><br /><a href=\"$deny_url\">$deny_url</a><br /><br />";
                $message .= "----------------------------------------------------------------------------------------------------------------------<br />";
                $message .= "<strong>Preuzimanje formulara.</strong><br />";
                $message .= "----------------------------------------------------------------------------------------------------------------------<br /><br />";
                $message .= "Formular koji ste ispunili, možete preuzeti u PDF formatu na sledećem linku:<br /><br />";
                $message .= "<a href=\"$form_url\">$form_url</a><br /><br />";
                $message .= "----------------------------------------------------------------------------------------------------------------------<br /><br />";
                $message .= "<strong>IP: ".$user_ip.'<br />';
                $message .= "<strong>BROWSER: ".$user_agent.'<br />';
                $message .= "<strong>Datum i vrijeme: ". date('Y-m-d H:i:s',time()).'<br /><br />';
                $message .= '-- <br /><div style="text-align:left">UDRUŽENJE "SPAJALICA" SARAJEVO</br>adresa: Grbavička 6a, 71 000 Sarajevo, Bosna i Hercegovina<br />web: <a target="_blank" href="http://www.spaja-lica.com">www.spaja-lica.com</a><br />e-mail: <a target="_blank" href="mailto:info@spaja-lica.com">info@spaja-lica.com</a><div>';

                $this->load->library('email');
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['send_multipart'] = FALSE;
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from('info@spaja-lica.com', 'Udruženje "Spajalica"');
                $this->email->to($email);
                $this->email->bcc('info@spaja-lica.com');
                $this->email->subject("Prijava za konkurs 2012-2013 - $name - $email");
                $this->email->message($message);

                if($this->email->send()) {
                    $this->email->clear();
                    $this->db->set('status',1)->where('session_id',$session_id)->update('konkursi');
                } else {
                    show_error($this->email->print_debugger());
                }
    }
    
    private function send_sms($to,$sms_kod,$id) {
        //Username: spajalica 
        //ClientID: AVU212
        $user = "spajalica";
        $password = "DavudAga!14";
        $api_id = "3397479";
        $baseurl ="http://api.clickatell.com/http/sendmsg";
        $from = 'spajalica';
        $text = urlencode("Kod za potvrdu:\n ".$sms_kod);
        
        $url = "$baseurl?user=$user&password=$password&api_id=$api_id&callback=3&from=$from&to=$to&text=$text";

        // do sendmsg call
        $ret = file($url);
        $send = explode(":",$ret[0]);

        if ($send[0] == "ID") {
            $this->db->set('sms_api_id',trim($send[1]))->where('id',$id)->update('konkursi');
            return true;
        } else { // vea add - seeaams ti doesn't work...
            $message = 'Greska prilikom slanja SMS-a za prijavu za konkurs. <br />';
            $message = 'To: '.$to.'<br />';
            $message .= 'ID: '.$id.'<br />';
            $message .= 'SMS kod: '.$sms_kod.'<br />';
            send_email('valajbeg@gmail.com', 'GRESKA NA SMS VALIDACIJI', $message);
            return false;
        }
    }
    
    function callbacks_sms_api() {
        // http://www.spaja-lica.com/konkursi/callbacks_sms_api?api_id=12345&apiMsgId=996f364775e24b8432f45d77da8eca47&cliMsgId=abc123&timestamp=1218007814&to=279995631564&from=27833001171&status=003&charge=0.300000
        $to = $this->input->get_post("to");
        $this->db->set('sms_status',$this->input->get_post("status"));
        $this->db->where('mobitel',$to);
        $this->db->where('sms_api_id',$this->input->get_post("apiMsgId"));
        $this->db->update('konkursi');
        return TRUE;
    }
    
    /* CHECK - FORM VALIDATION FUNCTIONS!!! */

    function check_godina_studija($godina,$trajanje_studija) {
        
        if ((int)$godina > (int)$trajanje_studija) {
            $this->form_validation->set_message('check_godina_studija', 'Nije moguće da studirate na višoj godini studija nego li Vaš studij traje!!');
            return FALSE;
        }
            return TRUE;
    }
    
    function check_mobile($mobile) {
            
            $allowed_cellphone_prefix = $this->config->item('allowed_cellphone_prefix');
            $mobile = trim($mobile);
            
            if (!in_array(substr($mobile, 0, 5), $allowed_cellphone_prefix)) {
                $this->form_validation->set_message('check_mobile', 'Broj mobitela mora pripadati jednom od BH telekom operatera!');
                return FALSE;
                }
            
            return TRUE;
    }

    function check_sms_kod($sms_kod,$session_id) {
            $valid_kod = $this->db->where('sms_kod',$sms_kod)->where('session_id',$session_id)->get('konkursi')->num_rows();

            if ($valid_kod != 0) {
                $this->db->set('status','4')->where('session_id',$session_id)->update('konkursi');
                return TRUE;
            } else {
                $this->form_validation->set_message('check_sms_kod', 'SMS kod nije validan!');
                return FALSE;
            }
    }
    
    function is_unique_active($val, $field) {
        
        $status = $this->db->where($field,$val)->where('status !=','-1')->get('konkursi')->num_rows();

            if ($status != 0) {
                    $this->form_validation->set_message('is_unique_active', "\"$field\" koji ste unjeli - $val - je već korišten. ");
                return FALSE;
            } else {
                return TRUE;
            }
    }
    
    function check_prosjek($prosjek,$godina) {
            if ($godina == 1 && $prosjek > 5) {
                $this->form_validation->set_message('check_prosjek', 'Nije moguće da ste prvi put redovno upisani prvu godinu studija, školske 2012-2013 godinu i da ima te prosjek: '.$prosjek);
                return FALSE;
            } elseif ($prosjek < 6 && $godina > 1) {
                $this->form_validation->set_message('check_prosjek', 'Prosjek koji ste naveli -  '.$prosjek.' - se ne poklapa sa ostalim unesenim podatcima o godini studija.');
                return FALSE;
            } else {
                return TRUE;
            }
    }
  
    function check_captcha() {
        $this->load->helper('captcha_functions'); return check_captcha(); //ne znam iz kojeg razloga nece da povuce sam funkciju iz helpera;
    }
    
    /*  Is konkurs valid?  */
    
    function _is_active(){
        $exp_date = '2012-11-16';
        
        if ($this->uri->segment(2)  == 'topdf') return true;
        if ($this->uri->segment(2)  == 'preview') return true;
//        if ($this->uri->segment(2)  == 'sms_validate') return true;
//        if ($this->uri->segment(2)  == 'email_validate') return true;
        
        if (strtotime(date('Y-m-d')) >= strtotime($exp_date)  && $this->uri->segment(2) != 'off') {
           redirect('http://www.spaja-lica.com/konkursi/off');
        }
    }
    
}
    