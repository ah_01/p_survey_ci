<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admini extends CI_Controller {

            public $view = Array(
                        'theend' => 'backend',
                        'layout' => 'one-column',
                        'mainbar' => array('dashboard'),
                        'sidebar' => array('default'),
                    );

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Europe/Sarajevo');
        //var_dump($this->session->all_userdata()); die();
        $this->_is_logged_in();
        $this->load->model('Admini_model', 'a');
        $this->load->helper('directory');
    }

    public function index() {
//        $this->output->enable_profiler(TRUE);

        $data['title'] = 'Dashboard';

        $data['categories'] = $this->a->get_categories();
        $data['settings'] = $this->db->get('settings')->result_array();
        $data['users'] = $this->db->get('users')->result_array();
        $data['sessions'] = $this->db->order_by('last_activity','desc')->limit(20)->get('sessions')->result_array();

        $data['content_all'] = $this->db->get('content')->num_rows();
        $data['content_online'] = $this->db->where('status', 1)->get('content')->num_rows();
        $data['content_offline'] = $this->db->where('status', 0)->get('content')->num_rows();
        $data['subscribers'] = '';

        $this->view['layout'] = 'two-columns-right';

        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function settings() {
        $data['title'] = 'Settings';
        $this->view['mainbar'] = 'settings';
        $this->view['layout'] = 'two-columns-right';

        // vea add - MUST NOT BE THIS MUCH DB REQUESTS!!!
        $data['path_to_layouts'] = $this->db->get_where('settings','key = "path_to_layouts"')->row()->value; // this querry style is not much cleaner!!!
        $data['path_to_article_layouts'] = $this->db->get_where('settings','key = "path_to_article_layouts"')->row()->value;
        $data['frontpage_slider'] = $this->db->get_where('settings','key = "frontpage_slider"')->row()->value;
        $data['contact_email'] = $this->db->get_where('settings','key = "contact_email"')->row()->value;
        $data['analytics_code'] = $this->db->get_where('settings','key = "analytics_code"')->row()->value;
        $data['main_view'] = $this->db->get_where('settings','key = "main_view"')->row()->value;
        $data['main_views'] = directory_map('./application/views',1); // vea add - very dirty!!!

        if ($this->uri->segment(3) == 'save') {
            if ($this->form_validation->run('settings_save') == FALSE) {
                    $this->form_validation->set_error_delimiters('<div class="error"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> ','</p></div></div>');
                   } else {
                       $this->a->save_settings();
                       redirect('/admini/settings');
                   }
        }

        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function content() {
        switch ($this->uri->segment(3)) {
            case 'search':
                    $term = $this->input->post('term');
                    if (strlen($term) <= 2) redirect('/admini/content');
                    $this->load->library('pagination');
                    $data['content_list'] = $this->db->like('title', $term, 'both')->or_like('fulltext', $term, 'both')->get('content')->result_array();
                    $data['title'] = 'Create/Edit article';
                    $data['categories'] = $this->a->get_categories();
                    $this->view['mainbar'] = 'search-result';
                break;
            case 'form':
                    $data['title'] = 'Create/Edit article';
                    $data['categories'] = $this->a->get_categories();
                    $this->view['mainbar'] = 'content-form';
                break;
            case 'save': # update or create content if it doesn't exist
                    $data['title'] = 'Create/Edit article';
                        if ($this->form_validation->run('content_save') == FALSE) {
                             $this->form_validation->set_error_delimiters('<div class="error"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> ','</p></div></div>');
                             $this->view['mainbar'] = 'content-form';
                             $this->view['sidebar'] = array('default','statistics');
                             $data['categories'] = $this->a->get_categories();
                        } else {
                            $this->a->save_content();
                            redirect('/admini/content');
                        }
                break;
            case 'edit':
                    $id = $this->uri->segment(4);
                    $data['title'] = 'Create/Edit article';
                    $data['content'] = $this->a->get_content($id);
                    $data['categories'] = $this->a->get_categories();
                    $this->view['mainbar'] = 'content-form';

                    // set last edit/open to a session
                    //$last[$data['content']['id']] = $data['content']['title'];

                    $last_in_session = $this->session->userdata('last_edit');
                    if(!isset($last_in_session) || $last_in_session == '') {
                        $last_in_session[$data['content']['id']] = $data['content']['title'];
                        $this->session->set_userdata('last_edit',$last_in_session);
                    } else {
                        $last_in_session[$data['content']['id']] = $data['content']['title'];
                        $this->session->set_userdata('last_edit',$last_in_session);
                    }

                break;
            case 'delete':
                $this->a->delete_content($this->uri->segment(4));
                redirect($this->agent->referrer());
                break;
            case 'status':
                $this->a->status_content($this->uri->segment(4));
                redirect($this->agent->referrer());
                break;
            default: # list or default :)
                $data['title'] = 'Content';

                $this->load->library('pagination');

                $config['base_url'] = base_url('admini/content/page');
                $config['total_rows'] = $this->db->get('content')->num_rows();
                $config['per_page'] = 100;
                $config['num_links'] = 10;
                $config['full_tag_open'] = '<div class="pagination">';
                $config['full_tag_close'] = '</div>';
                $config['uri_segment'] = 4;

                $data['content_list'] = $this->a->get_content('all',$this->uri->segment(4),$config['per_page']);
                $data['categories'] = $this->a->get_categories();

                $this->pagination->initialize($config);
                # sortiranje
                # pretraga
                $this->view['mainbar'] = 'content-list';
                break;
        }
        $this->view['layout'] = 'two-columns-right';
        $this->view['sidebar'] = array('default','statistics');
        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function categories() {
        $data['title'] = 'Categories';
        $this->view['mainbar'] = 'categories';
        $this->view['layout'] = 'two-columns-right';

        switch ($this->uri->segment(3)) {
            case 'save':
                if ($this->form_validation->run('category_save') == FALSE) {
                             $this->form_validation->set_error_delimiters('<div class="error"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> ','</p></div></div>');
                             $this->view['mainbar'] = 'categories';
                             $this->view['sidebar'] = array('default','statistics');
                        } else {
                            $this->a->save_category();
                            redirect('/admini/categories');
                        }
                break;
            case 'edit':
                $data['title'] = 'Edit Category';
                $data['category'] = $this->a->get_category($this->uri->segment(4));
                break;
            case 'delete':
                $data['category'] = $this->a->delete_category($this->uri->segment(4));
                redirect($this->agent->referrer());
                break;
            default:

                break;
        }

        $data['layouts'] = directory_map('.'.$this->db->where('key','path_to_layouts')->get('settings')->row()->value,1);
        $data['article_layouts'] = directory_map('.'.$this->db->where('key','path_to_article_layouts')->get('settings')->row()->value,1);
        $data['sidebar_content'] = $this->db->where('category_id','5')->where('status','1')->get('content')->result_array();
        $data['categories'] = $this->a->get_categories() ;
        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function survey()
    {
        # code...
    }

    public function browser() {
        $data['title'] = 'Browser';
        $this->view['content'] = 'mainbar/browser';
        $this->view['layout'] = 'one-column';
        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function demo() {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->session->all_userdata();
        $this->view['mainbar'] = 'demo';
        $this->view['layout'] = 'two-columns-right';
        $data['view'] = $this->view;
        $this->load->view('main', $data);
    }

    public function ajax() {

        $query = $this->input->get('term');

        switch ($this->uri->segment(3)) {
            case 'subject':
                $data = get_array_subnode($this->db->select('subject')->like('subject',$query,'both')->distinct()->get('content')->result_array(),'subject');
                break;
            case 'author':
                # ovo nece bas najbolje filtrirati
                $users =$this->db->select('CONCAT(name , " " , surname) as author',false)->like('name',$query,'both')->get('users')->result_array();
                $authors = $this->db->select('author')->like('author',$query,'both')->distinct()->get('content')->result_array();
                $data = get_array_subnode(array_merge($users,$authors),'author') ;
                break;
            case 'source':
                $data = get_array_subnode($this->db->select('source')->like('source',$query,'both')->distinct()->get('content')->result_array(),'source') ;
                break;
            case 'tags':
                #ovo nece dobro raditi jer ce pokupiti i tagove koji nisu trazeni, ali bar ce malo filtrirati sadrzaj
                $tagss = $this->db->select('tags')->like('tags',$query,'both')->distinct()->get('content')->result_array();
                $tmp = array();

                foreach ($tagss as $tags_string) {
                    # ovdje provjera koja je to vrijednost u sad vec konvertovanom stringu u niz, te samo zapisivanje vrijednosti sa tim key-em... PAZNJA!!! Moze biti vise takvih vrijednosti!!!
                    # vea todo - ovdje treba unserialize umjesto explode !?!?!? - vea add - ipak ne treba jer se orginalno snima string koji je samo odvojen zarezima
                    $tags_array = explode(',', $tags_string['tags']);
                    foreach ($tags_array as $value) {
                            if (is_int(strpos(strtolower($value), strtolower($query)))) {
                                if (!in_array($value, explode(',', $_GET['tags'])))
                                    array_push($tmp, $value);
                            }
                    }
                }
                    $data = array_values(array_unique($tmp));
                break;
            default:
                $data = array('foo'=>'haa');
                break;
        }
//        var_dump($data); die();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function _is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('login');
            die();
        }
    }

    function recalculate_konkursi() {
        // get all konkursi
        // get_bodovi method from Konkursi_model
        // update for ID nn polja:  bodovi & serialize(bodovanje)
        die();
        $this->load->model('Konkursi_model', 'konkurs');

        $this->db->trans_start();
        $aplikacije = $this->db->order_by('bodovi','desc')->get('konkursi')->result_array();

        foreach ($aplikacije as $aplikacija) {
            $bodovi = $this->konkurs->get_bodovi($aplikacija["trajanje_studija"], $aplikacija["godina_studija"],$aplikacija["prosjek_ocjena"],$aplikacija["ukupna_primanja"],$aplikacija["ukupno_clanova"],$aplikacija["roditelji"],$aplikacija["porodica_rvi"]);
            echo $aplikacija['id'].' - '.$bodovi['ukupno'].' - '.$bodovi['materijalno_stanje'];
            echo '<br />';
            print_r($bodovi);
            echo '<hr />';

            $izmjena = Array(
                'bodovi' => $bodovi['ukupno'],
                'bodovanje' => serialize($bodovi),
            );

            $this->db->where('id', $aplikacija['id']);
            $this->db->update('konkursi', $izmjena);
        }
            $this->db->trans_complete();
    }

}
