<?php 

class Survey extends CI_Controller
{
	

	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->library('Datatables');
            
        }

		public function index()
		{
			$this->load->view("survey/index");
		}

		public function forma()
		{
			$token = $this->input->post("token");
			//var_dump($token);
			$result = $this->db->where('token', $token)->get('survey_tokens')->result_array();
			
			//var_dump($result);

			if(empty($result)==1) {
				$message = "Token ne postoji!";
				$this->session->set_flashdata('message', $message);
				redirect(base_url("/survey/index"));			

			}


			if ($result[0]['status'] == 1) {
				$message = "Token je iskoristen";
				$this->session->set_flashdata('message', $message);
				redirect(base_url("/survey/index"));			

			}

			


			// var_dump($tokenstat->status);
			//die();

			if ( $token != '') {
        		$query = $this->db->where('token', $token)->where('status','0')->get('survey_tokens')->row_array();
				if ($this->form_validation->run('token') == FALSE && count($query) >= 1) {
					// provjere - baza
	                $this->form_validation->set_error_delimiters('<div class="error"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> ','</p></div></div>');
	                $data['error_message'] = 'Error, Try again.';
	         		$this->load->view("survey/index",$data);
	            } else {
            		$q = $this->db->where('id', $query['subject_id'])->get('survey_subjects');
            		$data['subject'] = $q->row();
            		$data['token'] = $token;
					$this->load->view("survey/survey",$data);
				}
			}


		}


		public function complete()
		{
            $this->form_validation->set_error_delimiters('<span style="color: #990000;"><img src="/images/icons/error.png" class="icon3" />','<br /></span>');
            if ($this->form_validation->run('survey') == TRUE) {
				$postData = $this->input->post();
				$array = array(
					"token"	=> $this->input->post('token'),
					"subject_id"	=> $this->input->post('subject_id'),
					"reply_1"	=> $this->input->post('q1'),
					"reply_2"	=> $this->input->post('q2'),
					"reply_3"	=> $this->input->post('q3'),
					"reply_4"	=> $this->input->post('q4'),
					"reply_5"	=> $this->input->post('q5'),
					"reply_6"	=> $this->input->post('q6'),
					"reply_7"	=> $this->input->post('q7'),
					"reply_8"	=> $this->input->post('q8'),
					"reply_9"	=> $this->input->post('q9'),
					"reply_10"	=> $this->input->post('q10'),
					"reply_11"	=> $this->input->post('q11'),
					"reply_12"	=> $this->input->post('q12'),
					"reply_13"	=> $this->input->post('q13'),
				);

				$this->db->insert('survey_results', $array);
				$this->db->where('token',$this->input->post('token'))->set('status','1')->update('survey_tokens');
				$this->load->view("survey/thankyou");	
		} else {

			$query = $this->db->where('token', $this->input->post('token'))->where('status','0')->get('survey_tokens')->row_array();
    		$q = $this->db->where('id', $query['subject_id'])->get('survey_subjects');
    		$data['subject'] = $q->row();
    		$data['token'] = $this->input->post('token');
            $this->load->view("survey/survey",$data);
		}
	}

		public function lista() ////datatables function for survey list table 
	{
		$this->datatables->select('id,code, subject, professor, year, department');
		// $this->datatables->where('id <', '10');
		$this->datatables->from('survey_subjects');
		// $results = $this->datatables->generate();
		// $results = $this->datatables->generate('json', 'ISO-8859-1');
		$results = $this->datatables->generate('raw');
		$data = array();
		
		$i = 0;
		foreach ($results['aaData'] as $key => $value) {
			$value['tokensUsed'] = $this->GetNumTokens($value['id']);
			$data[$i] = $value;
			$i++;
			// var_dump($value);
		}
		// var_dump($data);
		// var_dump($results['aaData']);

		$data['aaData'] = $data;
		// var_dump($data['aaData']);

		// $lista = $this->db->get('survey_subjects')->result_array();
		// 		//parse through array to sort it by id
		// foreach ($lista as $item) {
		// 	$array[$item['id']]= $item;
		// }
		// $data['lista'] = $array;
		// var_dump($data['lista']);



		// var_dump($data);
		$this->load->view("survey/list",$data);
	}

	private function GetNumTokens($id) //calculate num of used tokens for survey 
	{
		//$this->db->select();
		$res = $this->db->where('subject_id', $id)
					->select_sum('status')
					->get('survey_tokens')
					->row();

		// var_dump($res->status);			
		return $res->status;			
	}

	private function getSubjectData($id=115)
	{
		$res = $this->db->where('id', $id)
					->get('survey_subjects')
					->result();

		return $res;			
	}

		// get values for $n - question number, $id-subject_id $q-question value
		// and build data array for charts

	private function qData($n,$id = 114, $q = "Question 0") 
	{
		$qNum = $n;
		$qvalue = "q" . $qNum . "_value";


		$items = $this->db->where('subject_id', $id)
							->select('COUNT(id) AS count, reply_' . $qNum . ' as '. $qvalue .', (COUNT(id) * reply_' . $qNum . ') as total', false )
							->from('survey_results')
							->group_by('reply_'.$qNum)
							->get()->result();

			$data  = array();

			$totalAnswers = 0;
			$sum = 0;
			$brojOcjena = sizeof($items);
			$percentage = 0;


			foreach ($items as $item => $value){
				$sum+=$value->total;
				$totalAnswers+=$value->count;
			}


			foreach ($items as $item => $value) {
				# code...
				$data[$value->$qvalue]['grade']=$value->$qvalue;
				$data[$value->$qvalue]['count']=$value->count;
				$data[$value->$qvalue]['total']=$value->total;
				// $data[$value->q1_value]['Percent'] = ($sum/($value->total*100))*100;
				$data[$value->$qvalue]['percent'] = ($value->count/$totalAnswers)*100;
				
				
			}
		

			$data['res']['items'] = $data;
			$data['res']['total'] = $totalAnswers;
			$data['res']['brojOcjena'] = $brojOcjena;
			$data['res']['pitanje'] = $q;
			
			

		return $data;
	}

	public function report() // report - get id, and build charts and answeers based on it. Q are hardcoded - change it  - todo
	{
		$id = $this->uri->segment(3);

		$data['p'][0] = $this->qData(1, $id, "1. The class is well organised?");
		$data['p'][1] = $this->qData(2, $id, "2. I know what is expected of me in this class?");
		$data['p'][2] = $this->qData(3, $id, "3. The instructor seems well prepared for class.?");
		$data['p'][3] = $this->qData(4, $id, "4. The instructor explains clearly?");
		$data['p'][4] = $this->qData(5, $id, "5. There is sufficient time in class for questions/discussions during tutorials/labs?");
		$data['p'][5] = $this->qData(7, $id, "6. Course assignments, homework and quizzes are useful components of this course?");
		$data['p'][6] = $this->qData(7, $id, "7. The instructor is available for consultation outside of class?");
		$data['p'][7] = $this->qData(8, $id, "8. In this class I am treated equitably and with respect?");
		$data['p'][8] = $this->qData(9, $id, "9. The instructor is a good teacher, overall?");

		$comments = $this->db->select('reply_10, reply_11')
					->where('subject_id', $id)
					->from('survey_results')
					->get()->result();

		$subject = $this->getSubjectData($id);

		$data['comments']=$comments;			
		$data['subjectData']=$subject;
		$data['subjectData'][0]->usedTokens=$this->GetNumTokens($id);


		$this->load->view("survey/chart", $data);
	}

	public function jsonLista() // unused function for Json
	{
		$this->datatables->select('subject, professor, year, department');
		$this->datatables->where('id <', '10');
		$this->datatables->from('survey_subjects');
		// $results = $this->datatables->generate();
		$results = $this->datatables->generate('json', 'ISO-8859-1');
		// $results = $this->datatables->generate('raw');
		

		$data['aaData'] = $results['aaData'];
		echo($results);
		// $data['sColumns'] = $results['sColumns'];

		// var_dump($results);
	}

	public function tispis() //datatables function for tokens list table
	{
		$data['id'] = $this->uri->segment(3);
		
		$this->datatables->select('id,token, status');
		$this->datatables->where('subject_id', $data['id']);
		$this->datatables->from('survey_tokens');
		// $results = $this->datatables->generate();
		// $results = $this->datatables->generate('json', 'ISO-8859-1');
		$results = $this->datatables->generate('raw');
		$data['aaData'] = $results['aaData'];


		$data['subject'] = $this->db->where('id',$data['id'])->get('survey_subjects')->result_array();
		$data['page_title']=$data['subject'][0]['code'] . "-" . $data['subject'][0]['subject'];

		//  $data['lista'] = $this->db->where('subject_id', $data['id'])->get('survey_tokens')->result_array();
		 // var_dump($data);
		$this->load->view("survey/list",$data);

	}

	public function gentoken() // generate tokens function
	{
		//$this->db->empty_table('survey_tokens');
		
		$predmeti = $this->db->get('survey_subjects')->result_array();
		
		foreach ($predmeti as $predmet) {
			for ($i=0; $i < 50; $i++) { 
				$data = array(
				   'token' => strtoupper(random_string('alnum', 5)),
				   'subject_id' => $predmet['id'],
				   'status' => '0'
				);

				//empty table before insert

				$this->db->insert('survey_tokens',$data);
			}
		}

		var_dump("Done!");
	}
}