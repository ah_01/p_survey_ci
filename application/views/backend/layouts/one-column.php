<div class="content-header">
        <h2><?=$title?></h2>
        <p>
        <?php
            $day = Array('1'=> 'Ponedjeljak','2'=>'Utorak','3'=>'Srijeda','4'=>'Četvrtak','5'=>'Petak','6'=>'Subota','7'=>'Nedjelja');
            $month = Array('1'=> 'Januar','2'=>'Februar','3'=>'Mart','4'=>'April','5'=>'Maj','6'=>'Juni','7'=>'Juli','8'=>'Avgust','9'=>'Septembar','10'=>'Oktobar','11'=>'Novembar','12'=>'Decembar');
            echo $day[date('N',time())].', '.date('d. ',time()).$month[date('n',time())].date(' Y.',time());
        ?>
        </p>
</div>
<div class="line"></div>
<div class="single-column">
    <div class="mainbar">
        <?php
            if (isset($view['content']) && is_array($view['content'])) {
                foreach ($view['content'] as $content) {
                    $this->load->view('/backend/content/'.$content);
                }
            } elseif (isset($view['content'])) {
                    $this->load->view('/backend/content/'.$view['content']);
            }
        ?>
    </div>
</div>