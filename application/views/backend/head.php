<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>AdvanceIT Admin</title>

<!-- SSST ONLY FONTS STYLESHEETS -->
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|&subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|&subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Ubuntu|Titillium+Web' rel='stylesheet' type='text/css'>

<!-- Default CSS  -->
<link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/backend-styles.css" />

<!-- Jquery UI CSS  -->
<link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/jquery-ui-custom-theme/jquery-ui-1.8.20.custom.css" />

<!-- DateTime picker CSS  -->
<link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/jquery-ui-timepicker-addon.css" />

<!-- Load jQuery -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery-1.7.2.min.js"></script>

<!-- Load jQuery UI -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery-ui-1.8.20.custom.min.js"></script>

<!-- jQuery corners -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery.corner.js"></script>

<!-- Load jQuery time picker -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery-ui-timepicker-addon.js"></script>

<!-- Load Tagsinput JS jQuery Plugin-->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery.tagsinput.min.js"></script>

<!-- Default JS -->
<script type="text/javascript" src="<?=base_url()?>scripts/default.backend.js"></script>

<!-- Load TinyMCE -->
<script type="text/javascript" src="<?=base_url()?>scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=base_url()?>scripts/set-text-editor.js"></script>

<!-- KCFinder -->
<script type="text/javascript" src="<?=base_url()?>scripts/set-file-finder.js"></script>

<!-- Charts -->
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="<?=base_url()?>scripts/jqPlot/excanvas.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="<?=base_url()?>scripts/jqPlot/jquery.jqplot.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>scripts/jqPlot/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>scripts/jqPlot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>scripts/jqPlot/plugins/jqplot.donutRenderer.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url()?>scripts/jqPlot/jquery.jqplot.css" />

</head>
