<?php echo form_open('admini/settings/save'); ?>

<?php if (validation_errors() != "") : ?>
<div class="row">
               <?=form_label('Errors',"")?>
               <?=validation_errors()?>
</div>
<?php endif; ?>

<?php if (isset($category['id'])) { echo form_hidden('id',$category['id']); } else { echo form_hidden('id',set_value('id')); } ?>

<div class="row">
    <?php
        echo form_label("Web page status [main_view]: ","main_view");
        if (isset($main_view)) { $val = $main_view; } else { $val = set_value('main_view'); }
        $result = array();
        foreach ($main_views as $value) { if (substr($value,'-4',4) == '.php') $result[preg_replace("/\\.[^.\\s]{3,4}$/", "", $value)] = preg_replace("/\\.[^.\\s]{3,4}$/", "", $value); }
        echo form_dropdown('main_view', $result, $val,'class="box"');
    ?>
</div>

<div class="row">
    <?php
        echo form_label("Path to layouts [path_to_layouts]: ","path_to_layouts");
        if (isset($path_to_layouts)) { $val = $path_to_layouts; } else { $val = set_value('path_to_layouts'); }
        echo form_input('path_to_layouts',$val,'class="box" id=""');
    ?>
</div>

<div class="row">
    <?php
        echo form_label("Path to article layouts [path_to_article_layouts]: ","path_to_article_layouts");
        if (isset($path_to_article_layouts)) { $val = $path_to_article_layouts; } else { $val = set_value('path_to_article_layouts'); }
        echo form_input('path_to_article_layouts',$val,'class="box" id=""');
    ?>
</div>

<div class="row">
    <?php
        echo form_label("Contact Email [contact_email]: ","contact_email");
        if (isset($contact_email)) { $val = $contact_email; } else { $val = set_value('contact_email'); }
        echo form_input('contact_email',$val,'class="box" id=""');
    ?>
</div>

<div class="row">
    <?php # vea add - prljavo da ne moze prljavije!!!
            echo form_label('Frontpage Slider [frontpage_slider] <br /><small>Koristiti  "Ctrl" prilikom selektovanja.</small><small style="color: #900;">Images 870x300</small>',"frontpage_slider");
            if (isset($frontpage_slider)) { $val = implode(unserialize($frontpage_slider), "\n"); } else { $val = set_value('frontpage_slider'); }
            echo form_textarea('frontpage_slider',$val,'readonly="readonly" onclick="openKCFinderMultipleFiles(this,\'images\',\'/frontpage\')" class="box-wide" style="height: 100px;"');
            echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev" style="vertical-align: top;"');
    ?>

<?php
/*$postovi_list = array();
foreach ($arhiva_postovi as $item) { $postovi_list[$item['id']] = date('d-m-Y',  strtotime($item['datum_objave'])).' - '.$k[$item['kategorija_id']]['naslov'] . ': ' . $item['naslov']; }
$ip = unserialize($izdvojeno_postovi->value);
if (isset($izdvojeno_postovi)) { $val = $ip; } else { $val = set_value('izdvojeno_postovi'); }
// var_dump();
echo form_multiselect('izdvojeno_postovi[]', $postovi_list, $val,'style="width: 670px; height: 300px;"');
echo '<small>Koristeći tipku Ctrl + Klik miša, odabrati izdvojene članke. Odabrati min 5 max 20 izdvojenih članaka.</small>';*/
?>


</div>

<div class="row">
    <?php
            echo form_label("Google Analytics Code + other footer includes!</small>","analytics_code");
            if (isset($analytics_code)) { $val = $analytics_code; } else { $val = set_value('analytics_code'); }
            echo form_textarea('analytics_code',$val,'class="box4" style="height: 250px;" ');
    ?>
</div>

<div class="row" style="text-align: right;">
    <?php
            echo form_button('cancel','cancel','class="cancel" onclick="location.href=\'http://www.spaja-lica.com/admini/settings\'" ');
            echo form_submit('submit', 'Submit Form', 'class="submit"');
    ?>
</div>

<div class="row">
    <?php
   
echo '<div id="prilozi-div1" style="margin-bottom:4px;" class="clonedInput">';
echo form_input('title[]',"",'id="t1" style="display: inline; width: 570px; margin-right: 15px;"');
echo form_input('url[]',"",'id="u1" style="display: inline; width: 570px; margin-right: 15px;"');
echo form_input('image',$val,'id="i1" style="cursor: pointer; width: 603px" class="box-wide" readonly="readonly" onclick="openKCFinderIframe(this, \'upload\',\'images\')"  ');
echo form_button('p1', 'Odaberi fajl','style="padding: 1px 5px;" onclick="BrowseServer( \'Prilozi:/\', this.name );"');
echo '</div>';

?>

<div style="margin: 20px 0 0 0;">
    <input type="button" id="btnAdd" value="Dodaj slajd" style="padding: 1px 5px;" />
    <input type="button" id="btnDel" value="Ukloni poslednji slajd" style="padding: 1px 5px; margin: 0 10px;"/>
    <input type="button" id="btnValDel" value="Pobriši poslednje polje" style="padding: 1px 5px;" />
</div>
<input type="hidden" value="" name="counter" id="counter" />
</div>



<?php echo form_close(); ?>