<!-- FORM -->
<?php echo form_open('admini/content/save'); ?>

<div class="row">
<?php 
        if (validation_errors() != "") {
               echo form_label('Errors',"");
               echo validation_errors();
        }
?>
</div>

<div class="row">
<?php
    if (isset($content['id'])) {
            echo form_hidden('id',$content['id']);
            echo '<strong>HITS: '.$content['hits'].'</strong> | ';
            echo '<a href="'.base_url().'" target="_blank"><strong>ID: '.$content['id'].'</strong> - Pogledaj Članak<img src="/images/icons/popup2.png" class="icon2"></a>';
            } else {
            echo '<strong>HITS: 0</strong> | ';
            echo '<a href="#" target="_blank"><strong>ID: #</strong></a>';
            echo form_hidden('id',set_value('id')); 
        }
?>
</div>


<div class="row">
    <table  class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;">
                <?php
                    echo form_label("Status: ","status");
                    if (isset($content['status'])) {
                        $val = $content['status'];
                    } else {
                        $val = set_value('status');
                    }
                    $options = Array('0'=>'Draft','1'=>'Objavljeno','2'=>'Za pregledati i obijaviti');
                    echo form_dropdown('status', $options, $val,'class="box5"');
                ?>
            </td>
            <td style="padding-right: 0px;">
                <?php
                    echo form_label("Category: ","category_id");
                    if (isset($content['category_id'])) {
                        $val = $content['category_id'];
                    } else {
                        $val = set_value('category_id');
                    }
                    $options = get_array_subnode($categories,'name');
                    echo form_dropdown('category_id', $options, $val,'class="box5"');
                ?>
            </td>
            <td style="padding-right: 0px;">
                <?php
                    echo form_label("Language: ","language");
                    if (isset($content['language'])) {
                        $val = $content['language'];
                    } else {
                        $val = set_value('language');
                    }
                    $options = get_languages(TRUE);
                    echo form_dropdown('language', $options, $val,'class="box5"');
                ?>
            </td>
        </tr>
    </table>
</div>
    
<div class="row">
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;">
                <?php
                    echo form_label("Subject: ","subject");
                    if (isset($content['subject'])) {
                        $val = $content['subject'];
                    } else {
                        $val = set_value('subject');
                    }
                    echo form_input('subject',$val,'class="box4 autocomplete" id="subject"');
                ?>
            </td>
            <td>
                <?php
                    echo form_label("Source","source");
                    if (isset($content['source'])) {
                        $val = $content['source'];
                    } else {
                        $val = set_value('source');
                    }
                    echo form_input('source',$val,'class="box4 autocomplete" id="source" ');
                ?>
            </td>
            <td style="padding-right: 0px;">
                <?php
                    echo form_label("Author","author");
                    if (isset($content['author'])) {
                        $val = $content['author'];
                    } elseif (set_value('author') != '') {
                        $val = set_value('author');
                    } else {
                        $val = $this->session->userdata('name').' '.$this->session->userdata('surname');    
                    }
                    echo form_input('author',$val,'class="box4 autocomplete" id="author"');
                ?>
            </td>
        </tr>
    </table>
</div>
    
<div class="row">
        <?php
            echo form_label("Title: ","title");
            if (isset($content['title'])) {
                $val = $content['title'];
            } else {
                $val = set_value('title');
            }
            echo form_input('title',$val,'class="box"');
        ?>
</div>

<div class="row">
        <?php
            echo form_label("URL <small>(Ovo polje se automatski generiše, u normalnim okolnostima ga nebi trebalo mijenjati)</small>","url");
            if (isset($content['url'])) {
                $val = $content['url'];
            } else {
                $val = set_value('url');
            }
            echo form_input('url',$val,'class="box"');
        ?>
</div>
    
<div class="row">
        <?php # image select
            echo form_label("Image <small>(Click on field to browse server)</small>","image");
            if (isset($content['image'])) {
                $val = $content['image'];
            } else {
                $val = set_value('image');
            }
            echo form_input('image',$val,' style="cursor: pointer; width: 603px" class="box-wide" readonly="readonly" onclick="openKCFinderIframe(this, \'upload\',\'images\')"  ');
            echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev"');
            echo form_button('edit-image','EDIT',' class="reset-prev" id="reset-prev"');
        ?>
            <div id="kcfinder_iframe_div" style="display: none; position: absolute; z-index: 10; width: 670px; height: 400px; background: #e0dfde; border: 2px solid #30A4B1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; padding: 1px;"></div>
            <?php if ($val != '') echo '<img src="'.$val.'" class="preview-image" />'; ?>
</div>

<div class="row">
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;">
                <?php # treba po defaultu setovati danasnji datum
                    echo form_label("Date of create","date_create");
                    if (isset($content['date_create'])) { $val = $content['date_create']; } elseif (set_value('date_create') != '') { $val = set_value('date_create'); } else { $val = date('Y-m-d'); }
                    echo form_input('date_create',$val,'class="datepicker"');
                ?>
            </td>
            <td>
                <?php # treba po defaultu setovati danasnji datum
                    echo form_label("Date of update","date_update");
                    if (isset($content['date_update'])) { $val = $content['date_update']; } else { $val = set_value('date_update'); }
                    echo form_input('date_update',$val,'class="datepicker"');
                ?>
            </td>
            <td style="padding-right: 0px;">
                <?php # takodje danasnji datum ako nema drugog u bazi tj. ako mu je polje prazno - ovo treba jos razraditi konceptualno...
                    echo form_label("Date for order","date_order");
                    if (isset($content['date_order'])) { $val = $content['date_order']; } else { $val = set_value('date_order'); }
                    echo form_input('date_order',$val,'class="datepicker"');
                ?>
            </td>
        </tr>
    </table>
</div>

<div class="row">
    <?php
            echo form_label("Uvodni tekst <small>(do 700 karaktera) - [UKOLIKO NEMA INTRO TEXT-A, ISPISAT CE SE PRVIH NEKOLIKO REDOVA GLAVNOG TEKSTA]</small>","introtext");
            if (isset($content['introtext'])) {
                $val = $content['introtext'];
            } else {
                $val = set_value('introtext');
            }
            echo form_textarea('introtext',$val,'class="box4"');
    ?>
</div>

<div class="row">
    <?php
            echo form_label("Glavni text <small>(do 10000 karaktera)</small>","fulltext");
            if (isset($content['fulltext'])) {
                $val = $content['fulltext'];
            } else {
                $val = set_value('fulltext');
            }
            echo form_textarea('fulltext',$val,'id="tinymce" name="content" rows="15" cols="20" class="tinymce box"');
    ?>
</div>

<div class="row">
<?php
    echo form_label("Tags:  <small>(Nakon svakog taga otkucati zarez.)</small>","tags");
    if (isset($content['tags'])) {
        $val = $content['tags'];
    } else {
        $val = set_value('tags');
    }
    echo form_input('tags',$val,'class="box" id="tags" class="tags"');
?>
</div>

<div class="row">
    <?php # ovdje treba da se unserializuje kada se edituje content
            echo form_label('Attachments <br /><small>(Za više priloga, koristiti  "Ctrl" prilikom selektovanja. Potom desni klik na neki od selektovanih fajlova te izabrati "Select")</small>',"attachments");
            if (isset($content['attachments'])) {
                $val = implode(unserialize($content['attachments']), "\n");
            } else {
                $val = set_value('attachments');
            }
            echo form_textarea('attachments',$val,'readonly="readonly" onclick="openKCFinderMultipleFiles(this,\'upload\',\'attachments\')" class="box-wide" style="height: 100px;"');
            echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev" style="vertical-align: top;"');
    ?>
</div>

<div class="row">
    <?php # ovdje treba da se unserializuje kada se edituje content
            echo form_label('Gallery <br /><small>(Za više slika, koristiti  "Ctrl" prilikom selektovanja. Potom desni klik na neki od selektovanih fajlova te izabrati "Select")</small>',"gallery");
            if (isset($content['gallery'])) {
                $val = implode(unserialize($content['gallery']), "\n");
            } else {
                $val = set_value('gallery');
            }
            echo form_textarea('gallery',$val,'readonly="readonly" onclick="openKCFinderMultipleFiles(this,\'upload\',\'gallery\')" class="box-wide" style="height: 100px;"');
            echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev" style="vertical-align: top;"');
    ?>
</div>

<div class="row" style="text-align: right;">
    <?php
            echo form_submit('submit', 'Submit Form', 'class="submit"');
    ?>
</div>

<?php echo form_close(); ?>
