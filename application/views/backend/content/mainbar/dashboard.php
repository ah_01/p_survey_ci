<div class="row">
    <label for="">Categories list:</label>
          <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="width: 100%;">
                    <thead style="background-color: #efefef; font-weight: bolder; ">
                        <td width="20"><strong>ICO</strong></td>
                        <td width=""><strong>NAME</strong></td>
                        <td width="100"><strong>URL</strong></td>
                        <td width="20"><strong>APP</strong></td>
                        <td width="20"><strong>NL</strong></td>
                        <td width="90"><strong>LAYOUT</strong></td>
                        <td width="70"><strong>A.LAYOUT</strong></td>
                        <td width="60"><strong>EDIT/DELL</strong></td>
                    </thead>
                    <tbody>
            <?php foreach ($categories as $item) : ?>
                    <tr>
                        <td><small><img src="<?=($item['icon'] != '') ? $item['icon'] : '/images/icons/weather_clouds.png'?>" class="icon4" /></small></td>
                        <td><a href="/admini/categories/edit/<?=$item['id']?>"><?=$item['name']?></a></td>
                        <td><?=$item['url']?></td>
                        <td><?=$item['articles_per_page']?></td>
                        <td><?=$item['number_of_linx']?></td>
                        <td><?=$item['layout']?></td>
                        <td><?=$item['article_layout']?></td>
                        <td>
                                <a href="/admini/categories/edit/<?=$item['id']?>" class=""><img src="/images/icons/page_edit.png"  alt="Edit" title="Edit" class="icon2" /></a>
                                </a>
                                <a href="#" url="/admini/categories/delete/<?=$item['id']?>" class="confirm" action="[delete]" message="<?='<strong>'.htmlspecialchars($item['name'].'<br /><span style="color: #900; text-transform:uppercase;">Ovu operaciju može izvršiti samo glavni administrator!</span>').'</strong>'?>"><img src="/images/icons/page_delete.png"  alt="Delete" title="Delete" class="icon2" /></a>
                                <a href="<?=base_url().$item['url']?>" class="" target="_blank"><img src="/images/icons/popup.png"  alt="Preview" title="Preview" class="icon2" /></a>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
</div>

<div class="row">
    <label for="">Users overview:</label>
          <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="width: 100%;">
                    <thead style="background-color: #efefef; font-weight: bolder; ">
                        <td width=""><strong>NAME</strong></td>
                        <td width=""><strong>USERNAME</strong></td>
                        <td width=""><strong>EMAIL</strong></td>
                        <td width=""><strong>TELEPHONE</strong></td>
                        <td width=""><strong>ROLE</strong></td>
                    </thead>
                    <tbody>
            <?php foreach ($users as $item) : ?>
                    <tr>
                        <td><?=$item['name'].' '.$item['surname']?></td>
                        <td><?=$item['username']?></td>
                        <td><?=$item['email']?></td>
                        <td><?=$item['telephone']?></td>
                        <td><?=$item['role']?></td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
</div>

<div class="row">
    <label for="">Sessions overview: <small>(last sessions in past 24h)</small></label>
          <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="width: 100%;">
                    <thead style="background-color: #efefef; font-weight: bolder; ">
                        <td width="95"><strong>LAST ACT:</strong></td>
                        <td width=""><strong>IP ADDRESS</strong></td>
                        <td width=""><strong>USER</strong></td>
                        <td width=""><strong>USER AGENT</strong></td>
                    </thead>
                    <tbody>
            <?php foreach ($sessions as $item) : ?>
                    <tr>
                        <td style="color: #900;"><small><?=date('d.m.Y (H:i:m)',$item['last_activity'])?></small></td>
                        <td><a href="http://www.infosniper.net/?ip_address=<?=$item['ip_address']?>" target="_blank"><?=$item['ip_address']?></a></td>
                        <?php if ($item['user_data'] != '') $user = unserialize($item['user_data']); else $user['username'] = ''; ?>
                        <td><?=(isset($user['username'])) ? $user['username'] : ''?></td>
                        <td><?=$item['user_agent']?></td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
</div>

<div class="row">
<label for="">Settings overview: </label>
<table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="table-layout: fixed; width: 100%;">
              <thead style="background-color: #efefef; font-weight: bolder; ">
                  <td width="100"><strong>NAME:</strong></td>
                  <td width="100"><strong>KEY</strong></td>
                  <td width="50"><strong>TYPE</strong></td>
                  <td width="300"><strong>VALUE</strong></td>
              </thead>
              <tbody>
            <?php foreach ($settings as $item) : ?>
              <tr valign="top">
                  <td><?=$item['name']?></td>
                  <td><?=$item['key']?></td>
                  <td><?=$item['type']?></td>
                  <td width="300" style="overflow: auto; word-wrap: break-word; color: #900;"><?=htmlspecialchars($item['value'])?></div>
              </tr>
           <?php endforeach; ?>
          </tbody>
      </table>
</div>