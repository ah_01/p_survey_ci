<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>AdvanceIT Admin</title>

<!-- Default CSS  -->
<link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/backend-styles.css" />

<!-- Jquery UI CSS  -->
<link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/jquery-ui-custom-theme/jquery-ui-1.8.20.custom.css" />

<!-- Load jQuery -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery-1.7.2.min.js"></script>

<!-- Default JS -->
<script type="text/javascript" src="<?=base_url()?>scripts/default.js"></script>

<!-- Load jQuery UI -->
<script type="text/javascript" src="<?=base_url()?>scripts/jquery-ui-1.8.20.custom.min.js"></script>

<!-- Load TinyMCE -->
<script type="text/javascript" src="<?=base_url()?>scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=base_url()?>scripts/set-text-editor.js"></script>
<!-- /TinyMCE -->

<!-- KCFinder -->
<script type="text/javascript" src="<?=base_url()?>scripts/set-file-finder.js"></script>

</head>
    
<body>
<div class="wrapper">
<!-- HEADER - HEADER - HEADER - HEADER - HEADER - HEADER - HEADER - HEADER  -->
  <header>
        <h1 class="logo">admin@spaja-lica.com</h1>
        <p class="txt_right">Logged in as <strong>Davud Zukic </strong>  <span class="v_line"> | </span> <a href="#"> Logout</a></p>
        <!-- Navigation -->
        <div class="navbar navigation">
                    <ul class="menu">
                                <li><a href="#">WRITE</a></li>
                                <li>
                                    <a href="#" class="active">MANAGE</a>
                                    <ul class="menu-sub">
                                        <li><a href=""><img src="/images/icons/application_split.png" class="icon" />Opcija 1 itd..</a></li>
                                        <li><a href=""><img src="/images/icons/bug_error.png" class="icon" />Opcija 1 itd..</a></li>
                                        <li><a href=""><img src="/images/icons/page_white_tux.png" class="icon" />Opcija 1 itd..</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">SETTINGS</a></li>
                                <li><a href="#">USERS</a></li>
                        </ul>
                        <div id="searchform">
                                <form method="get" action="">
                                <input type="text" value="find something good..." class="search_box" name="search" onclick="this.value='';"  />
                                <input type="submit" class="search_btn" value="SEARCH" />
                                </form>
                        </div>
        </div>
        <div class="clear"></div>
    </header>
    <!-- /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER - /HEADER -->
    <!-- CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT - CONTENT  -->
    <div class="content">
                <!-- Intro -->
               <div class="content-header">
                                    <h2>Lets Enter The Dragon</h2>
                                    <p>Author <a href="#">Bruce Lee</a> | created 10-14-08</p>
                </div>
                <div class="line"></div>
                <div class="mainbar">
                        <!-- Checks -->
                        <div class="check_main">
                            <div class="check row">
                                    <div class="good"><img src="<?=base_url()?>images/check.gif" alt="check" class="icon" />Nice work <strong>Ninja Admin!</strong></div>
                            </div>
                            <div class="check row">
                                    <div class="bad"><img src="<?=base_url()?>images/x.gif" alt="check" class="icon" />You need more training, please <a href="#">try again</a>.</div>
                            </div>
                    </div>
                <!-- FORM -->
                     <div class="main-form">
                            <form id="form1" name="form1" method="post" action="">
                                <div class="row">
                                    <label for="">Labela: </label>
                                    <input type="text" name="name" class="box" />
                                </div>
                                <div class="row">
                                    <label for="">Labela: </label>
                                    <select name="date_end" class="box2" >
                                            <option selected="selected"> Bruce Lee</option>
                                            <option>Jackie Chan</option>
                                            <option>John Claude Van Damme</option>
                                            <option>Ben Johnson</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <label for="">Labela: </label>
                                    <textarea  id="tinymce" name="content" rows="15" cols="20" class="tinymce box" ></textarea>
                                </div>
                                <div class="row">
                                    <label for="">Labela Tabela: </label>
                                                        <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" >
                                                            <thead style="background-color:#d9d8d8; font-size:14px;">
                                                                <td width="179"><strong>USER</strong></td>
                                                                <td width="184"><strong>EMAIL</strong></td>
                                                                <td width="273"><strong>SOMETHING</strong></td>
                                                                <td width="50"><strong>Opt 1</strong></td>
                                                                <td width="50"><strong>Opt 2</strong></td>
                                                                <td width="100"><strong>DO IT</strong></td>
                                                            </thead>
                                                            <tbody>
                                                            <tr class="gray">
                                                                <td>Bruce Lee </td>
                                                                <td><a href="#">bruce@kungfu.com</a></td>
                                                                <td>Loriem ipsum dolor sit amet </td>
                                                                <td width="50">Opt 1</td>
                                                                <td width="50">Opt 2</td>
                                                                <td><a href="#">EDIT  </a><span class="v_line">| </span> <a href="#" class="delete">DELETE </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jackie Chan</td>
                                                                <td><a href="#">thechan@yahoo.com</a></td>
                                                                <td>Loriem ipsum dolor sit amet </td>
                                                                <td width="50">Opt 1</td>
                                                                <td width="50">Opt 2</td>
                                                                <td><a href="#">EDIT  </a><span class="v_line">| </span> <a href="#" class="delete">DELETE </a></td>
                                                            </tr>
                                                            <tr class="gray">
                                                                <td>John Claude Van Damme</td>
                                                                <td><a href="#">vandamme@gmail.com</a></td>
                                                                <td>Loriem ipsum dolor sit amet </td>
                                                                <td width="50">Opt 1</td>
                                                                <td width="50">Opt 2</td>
                                                                <td><a href="#">EDIT  </a><span class="v_line">| </span> <a href="#" class="delete">DELETE </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Ben Johnson </td>
                                                                <td><a href="#">ben@kungu.com</a></td>
                                                                <td>Loriem ipsum dolor sit amet </td>
                                                                <td width="50">Opt 1</td>
                                                                <td width="50">Opt 2</td>
                                                                <td><a href="#">EDIT  </a><span class="v_line">| </span> <a href="#" class="delete">DELETE </a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                </div>
                                <div class="row action-buttons">
                                        <input name="submit" type="submit" id="submit"  tabindex="5" class="com_btn" value="UPDATE" />
                                </div>
                                </form>
                    </div>
                    <div class="row">
                        <label for="">Select file PopUp: </label>
                        <input type="text" readonly="readonly" onclick="openKCFinderPopUp(this, 'images','')" value="Click here and select a file double clicking on it" class="box-wide" />
                        <input name="reset-prev" type="button" id="reset-prev"  class="reset-prev" value="RESET" />
                    </div>
                
                    <div class="row">
                        <label for="">Select file PopUp: </label>
                        <input type="text" readonly="readonly" onclick="openKCFinderPopUp(this, 'attachments','')" value="Click here and select a file double clicking on it" class="box-wide" />
                        <input name="reset-prev" type="button" id="reset-prev"  class="reset-prev" value="RESET" />
                    </div>
                
                    <div class="row">
                        <label for="">Select file PopUp: </label>
                        <input type="text" readonly="readonly" onclick="openKCFinderPopUp(this, 'banners','')" value="Click here and select a file double clicking on it" class="box-wide" />
                        <input name="reset-prev" type="button" id="reset-prev"  class="reset-prev" value="RESET" />
                    </div>
                
                    <div class="row">
                        <label for="">Select file Iframe: </label>
                        <input type="text" readonly="readonly" value="Click here to browse the server" onclick="openKCFinderIframe(this, 'images','')" style="cursor:pointer"  class="box-wide" />
                        <input name="reset-prev" type="button" id="reset-prev"  class="reset-prev" value="RESET" />
                        <div id="kcfinder_iframe_div" style="display: none; position: absolute; z-index: 10; width: 670px; height: 400px; background: #e0dfde; border: 2px solid #30A4B1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; padding: 1px;"></div>
                    </div>
                
                    <div class="row">
                        <label for="">Select Multiple Files: </label>
                        <textarea readonly="readonly" onclick="openKCFinderMultipleFiles(this,'attachments','')" class="box-wide" style="height: 150px;" >Click here and choose multiple files with the Ctrl/Command key. Then right click on one of them and choose "Select"</textarea>
                        <input name="reset-prev" type="button" id="reset-prev"  class="reset-prev" value="RESET" style="vertical-align: top;" />
                    </div>
                
                    <!-- start -->
                    <style type="text/css">
                    /*demo page css*/
                    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
                    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
                    </style>
                    
                    <!-- Accordion -->
                    <div class="row">
                        <label for="">Accordion: </label>
                            <div id="accordion">
                                <div>
                                <h3><a href="#">First</a></h3>
                                <div>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>
                                </div>
                                <div>
                                <h3><a href="#">Second</a></h3>
                                <div>Phasellus mattis tincidunt nibh.</div>
                                </div>
                                <div>
                                <h3><a href="#">Third</a></h3>
                                <div>Nam dui erat, auctor a, dignissim quis.</div>
                                </div>
                            </div>
                    </div>

                    <!-- Tabs -->
                    <div class="row">
                        <label for="">Tabs: </label>
                        <div id="tabs">
                                <ul>
                                        <li><a href="#tabs-1">First</a></li>
                                        <li><a href="#tabs-2">Second</a></li>
                                        <li><a href="#tabs-3">Third</a></li>
                                </ul>
                                <div id="tabs-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                <div id="tabs-2">Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum bibendum.</div>
                                <div id="tabs-3">Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor ullamcorper augue.</div>
                        </div>
                    </div>

                    <!-- Dialog NOTE: Dialog is not generated by UI in this demo so it can be visually styled in themeroller-->
                    <div class="row">
                        <label for="">Dialoga: </label>
                            <p>
                                    <a href="#" id="dialog_link" class="ui-state-default ui-corner-all" style="color: #EEE;">
                                    <span class="ui-icon ui-icon-newwin"></span>Open Dialog</a>
                            </p>
                    </div>

                    <!-- ui-dialog -->
                    <div class="row">
                        <label for="">Overlay and Shadow Classes <em>(not currently used in UI widgets)</em>: </label>
                            <div style="position: relative; width: 56%; height: 200px; padding:1% 4%; overflow:hidden;" class="fakewindowcontain">
                                    <p>Lorem ipsum dolor sit amet,  Nulla nec tortor. Donec id elit quis purus consectetur consequat. </p><p>Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. Aliquam ante. Suspendisse scelerisque dui nec velit. Duis augue augue, gravida euismod, vulputate ac, facilisis id, sem. Morbi in orci. </p><p>Nulla purus lacus, pulvinar vel, malesuada ac, mattis nec, quam. Nam molestie scelerisque quam. Nullam feugiat cursus lacus.orem ipsum dolor sit amet, consectetur adipiscing elit. Donec libero risus, commodo vitae, pharetra mollis, posuere eu, pede. Nulla nec tortor. Donec id elit quis purus consectetur consequat. </p><p>Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. Aliquam ante. Suspendisse scelerisque dui nec velit. Duis augue augue, gravida euismod, vulputate ac, facilisis id, sem. Morbi in orci. Nulla purus lacus, pulvinar vel, malesuada ac, mattis nec, quam. Nam molestie scelerisque quam. </p><p>Nullam feugiat cursus lacus.orem ipsum dolor sit amet, consectetur adipiscing elit. Donec libero risus, commodo vitae, pharetra mollis, posuere eu, pede. Nulla nec tortor. Donec id elit quis purus consectetur consequat. Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. Aliquam ante. </p><p>Suspendisse scelerisque dui nec velit. Duis augue augue, gravida euismod, vulputate ac, facilisis id, sem. Morbi in orci. Nulla purus lacus, pulvinar vel, malesuada ac, mattis nec, quam. Nam molestie scelerisque quam. Nullam feugiat cursus lacus.orem ipsum dolor sit amet, consectetur adipiscing elit. Donec libero risus, commodo vitae, pharetra mollis, posuere eu, pede. Nulla nec tortor. Donec id elit quis purus consectetur consequat. Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. </p>
                                    <!-- ui-dialog -->
                                    <div class="ui-overlay">
                                            <div class="ui-widget-overlay"></div>
                                            <div class="ui-widget-shadow ui-corner-all" style="width: 302px; height: 152px; position: absolute; left: 50px; top: 30px;"></div>
                                    </div>
                                    <div style="position: absolute; width: 280px; height: 130px;left: 50px; top: 30px; padding: 10px;" class="ui-widget ui-widget-content ui-corner-all">
                                            <div class="ui-dialog-content ui-widget-content" style="background: none; border: 0;">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                            </div>
                                    </div>
                            </div>
                         
                            <div id="dialog" title="Dialog Title">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                    </div>
                    
                    
                            <!-- Slider -->
                            <div class="row">
                                <label for="">Slider: </label>
                                <div id="slider"></div>
                            </div>

                            <!-- Datepicker -->
                            <div class="row">
                                    <label for="">Datepicker: </label>
                                    <div id="datepicker"></div>
                            </div>

                            <!-- Progressbar -->
                            <div class="row">
                                    <label for="">Progressbar: </label>
                                    <div id="progressbar"></div>
                            </div>

                            <!-- Highlight / Error -->
                            <div class="row">
                                <label for="">Highlight / Error: </label>
		<div class="ui-widget">
                                            <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                                                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                                                        <strong>Hey!</strong> Sample ui-state-highlight style.</p>
                                            </div>
                                        </div>
                                        <br>
		<div class="ui-widget">
                                                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                                                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                                    <strong>Alert:</strong> Sample ui-state-error style.</p>
                                                </div>
		</div>
                            </div>
                </div>        
    </div>
    <!-- /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - /CONTENT - -->    
    <!-- FOOTER - FOOTER - FOOTER - FOOTER -->
<footer>
    <div class="footer">
            <a href="http://www.advanceit.ba" target="_blank"><img src="<?=base_url()?>images/advanceit.jpg" /></a>
    </div>
  </footer>
    <!-- /FOOTER - /FOOTER - /FOOTER - /FOOTER -->    
</div>   
</body>
</html>

