<?php foreach ($articles as $article) : ?>
<article>
    <h1><a href="<?=base_url().$category['url'].'/'.$article['url']?>"><span class="subject"><?=$article['subject']?></span><?=$article['title']?></a></h1>
    
    <div class="row document-meta">
        <?php //($article['author'] != "") ?  "author: ".$article['author'].'&nbsp;&nbsp;|&nbsp;'  : "" ; echo ''; ?>
        <?=$this->lang->line('date')?>: <?=date("F jS, Y",  strtotime($article['date_update'])); ?>
        <?=($article['source'] != "") ?  '&nbsp;&nbsp;|&nbsp;'.'source: '.auto_link($article['source'], 'url', TRUE) : "" ;?>
    </div>

    <div class="row post-text" style="margin: 10px 0;">
            <?=$article['introtext']?>
    </div>
            <?php
            // vea add - FIX THIS SHIT ONLY FOR SSST
                // if (!empty($article['image'])) {
                //     $imgdata = @getimagesize(substr_replace(base_url(),"",-1).$article['image']);
                //     $width = ($imgdata[0] <= 400) ? '200' : '573';
                //     $style = ($imgdata[0] <= 400) ? '' : 'margin-right: 0px; margin-bottom: 0px;';
                //     $class = ($imgdata[0] <= 400) ? 'left' : '';
                //     echo '<img src="'.$article['image'].'" class="'.$class.'" alt="title" width="'.$width.'" style="'.$style.'" />';
                // }
            ?>
    <div class="row post-text">
            <?=$article['fulltext']?>
        <div class="hr clearfix"></div>
    </div>

    <div class="row col-xs-16 col-sm-16 col-md-16">
        <section>
            <?php
                    $gallery = unserialize($article['gallery']);
                    // vea add - the way it should not be verified!!!
                    if (strlen($gallery[0]) != 0) :
            ?>
            <h5>Gallery: </h5>
                <ul class="post-gallery" id="post-gallery" style="margin-left: 0px;">
                <?php foreach ($gallery as $image) : ?>
                    <li><a href="<?=substr_replace(base_url(),"",-1).$image?>" target="_blank"><img src="<?=substr_replace(base_url(),"",-1).$image?>" style="padding: 0px; margin: 0px; border: 0px;" /></a><span><?=$article['title']?></span></li>
                <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
                <div class="hr"></div>
            <?php endif; ?>
            <?php
                    $attachments = unserialize($article['attachments']);
                    if (strlen($attachments[0]) != 0) :
            ?>
            <h5>Attachments: </h5>
                <ul class="links">
                <?php foreach ($attachments as $attachment) : ?>
                    <?php $name = substr($attachment,(strrpos($attachment, '/')+1)); ?>
                    <li><a href="<?=$attachment?>" target="_blank"><img src="/images/icons/filetypes/<?=substr($name, strrpos($name, '.') + 1)?>.png" class="icon4" /><?=$name?></a></li>
                <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            <?php endif; ?>
        </section>
    </div>
</article>
<?php endforeach;   ?>