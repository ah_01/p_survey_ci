<style>
    .dropdown-menu li a {

        color: #FFFFFF !important;
    }

@media (max-width: 990px) {
    .navbar-header {
        float: none;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
        margin: 7.5px -15px;
    }
    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
}
</style>

<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav" id="header-menu">
        <div class="col-xs-8">
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              About SSST
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/static-page/introducing-ssst')?>">Introducing SSST</a> </li>
              <li> <a href="<?=base_url('/static-page/message-from-the-rector')?>">Message from the Rector</a> </li>
              <li> <a href="<?=base_url('/static-page/ssst-campus')?>">Services and Facilities</a> </li>
              <li> <a href="<?=base_url('/static-page/university-management')?>">University Management</a> </li>
              <li> <a href="<?=base_url('/static-page/a-partnership-in-excellence')?>">University of Buckingham</a> </li>
              <li> <a href="<?=base_url('/static-page/why-sarajevo')?>">Why Sarajevo</a> </li>
              <li> <a href="<?=base_url('/static-page/contact-us')?>">Contact us</a> </li>
           </ul>
        </li>
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Departments
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/static-page/computer-science-department')?>">Computer Science</a> </li>
              <li> <a href="<?=base_url('/static-page/department-of-information-systems')?>">Information Systems</a> </li>
              <li> <a href="<?=base_url('/static-page/department-of-economics')?>">Economics</a> </li>
              <li> <a href="<?=base_url('/static-page/political-science-and-international-relations-department')?>">Political Science and International Relations</a> </li>
              <li> <a href="<?=base_url('/static-page/modern-languages-department')?>">Modern Languages</a> </li>
              <li> <a href="<?=base_url('/static-page/sarajevo-film-academy')?>">Sarajevo Film Academy</a> </li>
              <li> <a href="<?=base_url('/static-page/department-of-engineering-science')?>">Engineering Sciences</a> </li>
              <li> <a href="<?=base_url('/static-page/medical-studies-in-english')?>">Medical School</a> </li>
           </ul>
        </li>
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Current Students
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/static-page/academic-calendar')?>">Academic Calendar</a> </li>
              <li> <a href="<?=base_url('/static-page/student-services')?>">Student Services</a> </li>
              <li> <a href="<?=base_url('/static-page/internship-opportunities')?>">Internships</a> </li>
              <li> <a href="<?=base_url('/static-page/it-office')?>">IT Office</a> </li>
              <li> <a href="<?=base_url('/static-page/library')?>">Library</a> </li>
              <li> <a href="<?=base_url('/static-page/student-activities-and-clubs')?>">Student Activities and Clubs</a> </li>
              <li> <a href="<?=base_url('/static-page/ssst-alumni')?>">Alumni</a> </li>
           </ul>
        </li>
        </div>
        <div class="col-xs-8">
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Prospective Students
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/static-page/degree-programmes')?>">Degree Programmes</a> </li>
              <li> <a href="<?=base_url('/static-page/applying-to-ssst')?>">Applying to SSST</a> </li>
              <li> <a href="<?=base_url('/static-page/entrance-exams-2014-2015')?>">Entrance Exams</a> </li>
              <li> <a href="<?=base_url('/static-page/financing-your-education-at-ssst')?>">Fees, Loans and Scholarships</a> </li>
              <li> <a href="<?=base_url('/static-page/open-day')?>">Open Day</a> </li>
           </ul>
        </li>
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Research
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/static-page/fostering-a-culture-of-research-and-development')?>">About Research at SSST</a> </li>
              <li> <a href="<?=base_url('/static-page/research-institutes')?>">Research Institutes</a> </li>
              <li> <a href="<?=base_url('/static-page/projects-and-collaborations')?>">Projects and Collaborations </a> </li>
              <li> <a href="<?=base_url('/static-page/recent-publications')?>">Recent Publications</a> </li>
              <li> <a href="<?=base_url('/static-page/honorary-chairs-and-professorships')?>">Honorary Chairs and Professorships</a> </li>
              <li> <a href="<?=base_url('/static-page/met-foundation')?>">MET Foundation</a> </li>
           </ul>
        </li>
        <li class="dropdown">
           <a href="#" class="dropdown-toggle" style="margin-right: 0px;" data-toggle="dropdown">
              News and Events
           </a>
           <ul class="dropdown-menu sub-menu" role="menu">
              <li> <a href="<?=base_url('/news')?>">Latest News</a> </li>
              <li> <a href="<?=base_url('/events')?>">Events</a> </li>
              <li> <a href="#">Press Releases</a> </li>
              <li> <a href="<?=base_url('/media-library')?>">Media Library</a> </li>
              <li> <a href="<?=base_url('/static-page/ssst-independent')?>">SSST Independent</a> </li>
              <li> <a href="<?=base_url('/static-page/calls-for-applications-and-vacancies')?>">Calls for Applications and Vacancies</a> </li>
           </ul>
        </li>
</div>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
</div>


<div class="clearfix"></div>


<nav class="navbar navbar-default" role="navigation">
   <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" 
         data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </button>
   </div>
   <div class="collapse navbar-collapse" id="example-navbar-collapse">
      <ul class="nav navbar-nav">
         <li class="active"><a href="#">iOS</a></li>
         <li><a href="#">SVN</a></li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               Java <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
               <li><a href="#">jmeter</a></li>
               <li><a href="#">EJB</a></li>
               <li><a href="#">Jasper Report</a></li>
               <li class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li class="divider"></li>
               <li><a href="#">One more separated link</a></li>
            </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Java <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                   <li><a href="#">jmeter</a></li>
                   <li><a href="#">EJB</a></li>
                   <li><a href="#">Jasper Report</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                   <li class="divider"></li>
                   <li><a href="#">One more separated link</a></li>
                </ul>
             </li>
      </ul>
   </div>
</nav>