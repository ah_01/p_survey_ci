<?php
    if (isset($page_title) && isset($page_description)) {
        echo "<h1>$page_title<span>$page_description</span></h1><hr />";
    }
?>
  <div id="main" class="clearfix">
    <div class="grid3">
      <h2>Blue</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../blue/colors.html">View Demo</a>
      </p>
      <p><a href="../blue/colors.html"><img src="images/illustrations/blue.jpg" alt="small" width="270" /></a></p> </div>
    <div class="grid3">
      <h2>Red</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../red/colors.html">View Demo</a>
      </p>
      <p><a href="../red/colors.html"><img src="images/illustrations/red.jpg" alt="small" /></a></p> </div>
    <div class="grid3 reset">
      <h2>Orange</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../orange/colors.html">View Demo</a>
      </p>
      <p><a href="../orange/colors.html"><img src="images/illustrations/orange.jpg" alt="small" /></a></p> </div>
      
      <div class="grid3">
      <h2>Green</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../green/colors.html">View Demo</a>
      </p>
      <p><a href="../green/colors.html"><img src="images/illustrations/green.jpg" alt="small" width="270" /></a></p> </div>
      
      <div class="grid3">
      <h2>Purple</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../purple/colors.html">View Demo</a>
      </p>
      <p><a href="../purple/colors.html"><img src="images/illustrations/purple.jpg" alt="small" width="270" /></a></p> </div>
      
      <div class="grid3 reset">
      <h2>Pink</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>
      <p>
      <a href="../pink/colors.html">View Demo</a>
      </p>
      <p><a href="../pink/colors.html"><img src="images/illustrations/pink.jpg" alt="small" width="270" /></a></p> </div>
  </div>