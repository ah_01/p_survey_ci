<div id="main" class="clearfix">
    
    <div class="grid2">
      <h2>Lorem ipsum dolor sit</h2>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et Sed ut perspiciatis </p>
      <form action="" method="post">
        <ul>
          <li>
            <label for="name">Name<span>(required)</span></label>
            <input name="name" id="name" type="text" required="" placeholder="Your firstname and lastname" autofocus="" class="required">
          </li>
          <li>
            <label for="company">Company</label>
            <input name="company" id="company" type="text" placeholder="Company name">
          </li>
          <li>
            <label for="email">E-mail<span>(required)</span></label>
            <input name="email" id="email" type="email" placeholder="Your e-mail address">
          </li>
          <li>
            <label for="subject">Subject<span>(required)</span></label>
            <input name="subject" id="subject" type="text" placeholder="Message subject">
          </li>
          <li>
            <label for="message">Message<span>(required)</span></label>
            <textarea name="message" id="message" placeholder="Message subject"></textarea>
          </li>
          <li>
            <button>Send</button>
          </li>
        </ul>
      </form>
    </div>
    
    <div class="grid3 reset vcard">
      	<h2>Our Office</h2>
		<div class="adr">
      		<div class="street-address">48 Cambridge Street</div>  
      		<span class="locality">Melbourne</span>,
    		<span class="region">Victoria</span>,
    		<span class="postal-code">3056</span>
    		<span class="country-name">Australia</span>
    	</div>
		<p>
			<strong>Telephone:</strong> <span class="tel">0800-123456</span><br>
			<strong>Email:</strong> <a href="mailto:info@yourcompany.com" class="email">info@yourcompany.com</a>
      </p>
      <h2>Our Location</h2>
      <img src="/images/illustrations/map.jpg">
  </div>
</div>