<?php $uri=$this->uri->segment(2); $language = $this->session->userdata('language'); ?>

<?php if ($language == 'bosnian') :?>
<div class="hidden-xs hidden-sm">
    <div class="col-xs-16 ssst-blue ssst-row">
            <div class="row ssst-blue pad ssst-border-right">
                <small><?=$this->lang->line('news')?></small>
                <h4><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>"><?=word_limiter($news[0]["title"],7)?></a></h4>
                <small><?=date('l j. F Y.',strtotime($news[0]["date_update"]))?></small>
                <span><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">....</a></span>
            </div>
            <div class="row ssst-border-top ssst-border-right ssst-box-height">
                    <a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">
                        <img src="<?=base_url($news[0]["image"])?>" alt="" width="100%" height="100%">
                    </a>
            </div>
    </div>

    <div class="col-xs-16 ssst-row ssst-red ssst-border-top ssst-box-height ssst-center" style="padding-top: 40px; width: 100%; text-decoration: blink; font-size: 18px;">
            <a href="<?=base_url()?>static-page/prijava-za-upis-na-ssst">
                <i class="fa fa-university fa-1x"></i> Prijava za upis na SSST
             </a>
    </div>

    <div class="col-xs-16 ssst-border-top">
        <div class="row">
            <div class="col-xs-8 ssst-blue ssst-border-right ssst-box-height ssst-center " style="padding: 30px 2px 0px 5px; text-align: center;">
                <a href="<?=base_url('static-page/primjeri-prijemnih-ispita')?>">
                    <span style="font-size: 28px; font-weight: bold; float: left; float: left;">PPI</span>
                    <span style="font-size: 12px; float: right; width: 62px; text-align: left;">Primjeri Prijemnih Ispita</span>
                    <div class="clearfix"></div>
                </a>
            </div>
            <div class="col-xs-8 ssst-blue ssst-box-height ssst-center " style="padding-top: 15px;">
                    <a href="http://www.buckingham.ac.uk" target="_blank">
                        <img src="/images/buckingham.png" alt="" height="40"><br>
                        <span style="font-size: 10px;">THE UNIVERSITY OF</span> <h4 style="margin: 5px 0">BUCKINGHAM</h4>
                    </a>
            </div>
        </div>
    </div>

    <div class="col-xs-16">
        <div class="row ssst-border-top">
            <div class="col-xs-8 ssst-box-height ssst-center" style="">
                <a href="<?=base_url('static-page/ssst-kampus')?>">
                    <img src="/images/campus.mini.jpg" alt="" width="100%" height="100%" border="0">
                </a>
            </div>
            <div class="col-xs-8 ssst-gray ssst-box-height ssst-center" style="padding-top: 45px;">
                <a href="http://filmfactory.ba/" target="_blank" style="color: #FFFFFF !important;">
                    film.factory
                </a>
            </div>
        </div>
    </div>

    <div class="col-xs-16 ssst-border-top">
        <a href="<?=base_url('/static-page/studij-medicine-na-engleskom-jeziku')?>">
            <img src="/images/medical-banner.jpg" alt="" width="100%" height="100%" border="0" style="">
            <h2 style=" position: absolute; top: -12px; left: 36px; width: 100%; ">
                <span style="color: white; font-size: 18px; padding: 5px 8px; background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                    Studij medicine @ SSST
                </span>
            </h2>
        </a>
    </div>

</div>

<?php else : ?>

<div class="hidden-xs hidden-sm">
    <div class="col-xs-16 ssst-blue ssst-row">
            <div class="row ssst-blue pad ssst-border-right">
                <small><?=$this->lang->line('news')?></small>
                <h4><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>"><?=word_limiter($news[0]["title"],7)?></a></h4>
                <small><?=date('l j. F Y.',strtotime($news[0]["date_update"]))?></small>
                <span><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">....</a></span>
            </div>
            <div class="row ssst-border-top ssst-border-right ssst-box-height">
                    <a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">
                        <img src="<?=base_url($news[0]["image"])?>" alt="" width="100%" height="100%">
                    </a>
            </div>
    </div>

    <div class="col-xs-16 ssst-row ssst-red ssst-border-top ssst-box-height ssst-center" style="padding-top: 40px; width: 100%; text-decoration: blink; font-size: 18px;">
            <a href="<?=base_url('/static-page/applying-to-ssst')?>">
                    <i class="fa fa-university fa-1x"></i> Applying to SSST
            </a>
    </div>

    <div class="col-xs-16">
        <div class="row ssst-border-top">
            <div class="col-xs-8 ssst-blue ssst-border-right ssst-box-height ssst-center " style="padding: 30px 2px 0px 5px; text-align: center;">
                <a href="<?=base_url('/static-page/samples-of-entrance-exams')?>">
                    <span style="font-size: 28px; font-weight: bold; float: left; float: left;">SEE</span>
                    <span style="font-size: 12px; float: right; width: 62px; text-align: left;">Sample of Entrance Exams</span>
                    <div class="clearfix"></div>
                </a>
            </div>
            <div class="col-xs-8 ssst-blue ssst-box-height ssst-center " style="padding-top: 15px;">
                    <a href="http://www.buckingham.ac.uk" target="_blank">
                        <img src="/images/buckingham.png" alt="" height="40"><br>
                        <span style="font-size: 10px;">THE UNIVERSITY OF</span> <h4 style="margin: 5px 0">BUCKINGHAM</h4>
                    </a>
            </div>
        </div>
    </div>

    <div class="col-xs-16">
        <div class="row ssst-border-top">
            <div class="col-xs-8 ssst-box-height ssst-center" style="">
                <a href="<?=base_url('static-page/primjeri-prijemnih-ispita')?>">
                    <img src="/images/campus.mini.jpg" alt="" width="100%" height="100%" border="0">
                </a>
            </div>
            <div class="col-xs-8 ssst-gray ssst-box-height ssst-center" style="padding-top: 45px;">
                <a href="http://filmfactory.ba/" target="_blank" style="color: #FFFFFF !important;">
                    film.factory
                </a>
            </div>
        </div>
    </div>

    <div class="col-xs-16 ssst-border-top ssst-gray" style="">
        <a href="<?=base_url('/static-page/medical-studies-in-english')?>" style="position: relative; width: 100%;">
            <img src="/images/medical-banner.jpg" alt="" width="100%" height="100%" border="0" style="">
            <h2 style=" position: absolute; top: -12px; left: 36px; width: 100%; ">
                <span style="color: white; font-size: 18px; padding: 5px 8px; background: #333;  filter: alpha(opacity=80); -moz-opacity: 0.80; -khtml-opacity: 0.80; opacity: 0.80;">
                    Medical Studies @ SSST
                </span>
            </h2>
        </a>
    </div>

</div>

<?php endif; ?>