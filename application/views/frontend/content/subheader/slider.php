<?php $uri=$this->uri->segment(2); $language = $this->session->userdata('language'); ?>

<?php if ($language == 'bosnian') :?>
<div class="slider row">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
	  </ol>
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">

	    <div class="item active">
	      <a href="#"><img src="/media/10tha/intro.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/obiljezavanje-desetogodisnjice-rada')?>">Obilježavanje desetogodišnjice rada</a>
                	</h1>
                </div>
	      </div>
	    </div>


	    <div class="item">
	      <a href="#"><img src="images/medschool_image.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/static-page/studij-medicine-na-engleskom-jeziku')?>">Studij medicine na engleskom jeziku</a>
                	</h1>
                </div>
	      </div>
	    </div>

	    <div class="item">
	      <a href="#"><img src="images/convocation-2013.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/ssst-prvi-akreditovani-univerzitet-u-bih')?>">SSST prvi akreditovani univerzitet u BiH</a>
                	</h1>
                </div>
	      </div>
	    </div>

	    <div class="item">
	      <a href="#"><img src="images/Prince-Salman.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/svecano-otvorena-katedra-za-mirovne-i-internacionalne-studije')?>">Svečano otvorena Katedra za mirovne i internacionalne studije</a>
                	</h1>
                </div>
	      </div>
	    </div>

<!-- 		<div class="item">
	      <a href="#"><img src="images/Margaret-Thatcher.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/centralni-ssst-ov-amfiteatar-ce-nositi-ime-lady-thatcher')?>">Centralni SSST-ov amfiteatar će nositi ime Lady Thatcher</a>
                	</h1>
                </div>
	      </div>
	    </div> -->

	  </div>
	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
</div>

<?php else : ?>

<div class="slider row">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
	    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
	  </ol>
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">

		<div class="item active">
	      <a href="#"><img src="/media/10tha/intro.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/tenth-anniversary-celebration')?>">Tenth anniversary celebration</a>
                	</h1>
                </div>
	      </div>
	    </div>

		<div class="item">
	      <a href="#"><img src="images/medschool_image.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/static-page/medical-studies-in-english')?>">Medical Studies in English</a>
                	</h1>
                </div>
	      </div>
	    </div>

	    <div class="item">
	      <a href="#"><img src="images/eng_certificates.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/certificates-awarded-to-english-course-participants')?>">Certificates Awarded to English Course Participants</a>
                	</h1>
                </div>
	      </div>
	    </div>

	    <div class="item">
	      <a href="#"><img src="images/convocation-2013.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/ssst-first-university-to-receive-institutional-accreditation')?>">SSST First University to Receive Institutional Accreditation</a>
                	</h1>
                </div>
	      </div>
	    </div>

	    <div class="item">
	      <a href="#"><img src="images/Prince-Salman.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/inauguration-of-prince-salman-chair-in-peace-and-international-studies')?>">Inauguration of Prince Salman Chair in Peace and International Studies</a>
                	</h1>
                </div>
	      </div>
	    </div>

		<!-- <div class="item">
	      <a href="#"><img src="images/Margaret-Thatcher.jpg" alt="..."></a>
	      <div class="carousel-caption">
                <div class="bg-h1">
                	<h1>
                		<a href="<?=base_url('/news/centralni-ssst-ov-amfiteatar-ce-nositi-ime-lady-thatcher')?>">Central SSST Auditorium Named After Lady Thatcher</a>
                	</h1>
                </div>
	      </div> 
	    </div>-->

	  </div>
	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
</div>
<?php endif; ?>
