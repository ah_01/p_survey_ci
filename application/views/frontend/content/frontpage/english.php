<div class="content">
	<div class="row ssst-row">
		<div class="col-md-4 col-xs-16">
			<div class="row ssst-blue ssst-sub-row pad ssst-border-right">
				<small><?=$this->lang->line('news')?></small>
				<h4><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>"><?=word_limiter($news[0]["title"],7)?></a></h4>
				<small><?=date('l j. F Y.',strtotime($news[0]["date_update"]))?></small>
				<span><a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">....</a></span>
			</div>
			<div class="row ssst-border-top ssst-border-right ssst-box-height">
					<a href="<?=base_url($categories[$news[0]["category_id"]]['url'].'/'.$news[0]["url"])?>">
						<img src="<?=base_url($news[0]["image"])?>" alt="" width="100%" height="100%">
					</a>
			</div>
		</div>
		<div class="col-md-6 col-xs-16">
			<div class="row">
				<div class="col-xs-5 ssst-red ssst-box-height ssst-center pad ssst-border-bottom ssst-border-right" style="width: 33.3%; padding-top: 25px;">
						<a href="<?=base_url('/static-page/graduate-studies-2014')?>"><i class="fa fa-graduation-cap fa-2x"></i><br>Graduate Studies</a>
				</div>
				<div class="col-xs-5 ssst-blue ssst-box-height ssst-center pad ssst-border-bottom ssst-border-right" style="padding-top: 40px; width: 33.3%">
					<a href="<?=base_url('/static-page/contact-us')?>">Contact Us</a>
				</div>
				<div class="col-xs-5 ssst-blue ssst-box-height ssst-center ssst-border-bottom ssst-border-right" style="width: 33.3%; padding: 30px 5px 0 5px;">
					<a href="<?=base_url('/static-page/samples-of-entrance-exams')?>">
	                    <span style="font-size: 23px; margin-top: 8px; font-weight: bold; float: left; float: left;">SEE</span>
    	                <span style="font-size: 12px; float: right; width: 62px; text-align: left;">Sample of Entrance Exams</span>
        	            <div class="clearfix"></div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5 ssst-blue ssst-box-height ssst-center pad ssst-border-right" style="width: 33.3%; padding-top: 15px;">
					<a href="http://www.buckingham.ac.uk" target="_blank">
						<img src="/images/buckingham.png" alt="" height="40"><br>
						<span style="font-size: 10px;">THE UNIVERSITY OF</span> <h4 style="margin: 5px 0">BUCKINGHAM</h4>
					</a>
				</div>
				<div class="col-xs-5 ssst-box-height ssst-border-right" style="width: 33.3%">
					<a href="<?=base_url('static-page/ssst-campus')?>">
						<img src="/images/campus.jpg" alt="" width="100%" height="100%">
					</a>
				</div>
				<div class="col-xs-5 ssst-box-height" style="width: 33.3%">
					<a href="<?=base_url('static-page/enter')?>">
						<img src="/images/image4.jpg" alt="" width="100%" height="100%">
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-16">
			<div class="row ssst-red ssst-box-height ssst-center ssst-border-bottom" style="padding-top: 35px; width: 100%; text-decoration: blink; font-size: 25px;">
				<a href="<?=base_url('/static-page/applying-to-ssst')?>">
					<i class="fa fa-university fa-1x"></i> Applying to SSST
				</a>
			</div>
			<div class="row ssst-box-height ssst-center ssst-border-bottom" style="padding-top: 35px; width: 100%;  background: #282828; color: #FFFFFF; text-decoration: blink; font-size: 25px;">
				<a href="http://filmfactory.ba/" target="_blank" style="color: #FFFFFF !important;">
					film.factory
					<!-- <img src="/images/film.factory.jpg" alt="Film Factory" width="100%" height="100%"> -->
				</a>
			</div>
		</div>
	</div>
</div>

<div class="content clearfix ssst-row">
	<div class="col-md-10 col-xs-16 ssst-no-padding-left" style="height: 220px;">
			<a href="<?=base_url('/static-page/scholarships-2014')?>"><img src="images/50scholarships.fp.jpg" alt="" width="100%" height="100%"></a>
	</div>

	<div class="col-md-6 col-xs-16 ssst-no-padding-right">
		<div class="ssst-sub-row ssst-blue pad">
				<small><?=$this->lang->line('events')?></small>
				<h4><a href="<?=base_url($categories[$events[0]["category_id"]]['url'].'/'.$events[0]["url"])?>"><?=word_limiter($events[0]["title"],12)?></a></h4>
				<small><?=date('l j. F Y.',strtotime($events[0]["date_update"]))?></small>
				<span><a href="<?=base_url($categories[$events[0]["category_id"]]['url'].'/'.$events[0]["url"])?>">....</a></span>	
		</div>
		<div class="ssst-border-top ssst-sub-row" style="">
			<a href="<?=base_url('/static-page/conference-center')?>">
				<img src="images/image8.jpg" alt=""  width="100%" height="100%">
			</a>
		</div>
	</div>
</div>

<div class="content clearfix ssst-row ssst-sub-row">
	<div class="col-xs-16 col-md-6 ssst-no-padding-left" style="">
		<img src="<?=base_url($media_library[0]["image"])?>" alt="" style="width: 100%;" height="100%">
	</div>
	<div class="col-xs-16 pad col-md-10 ssst-no-padding-right ssst-red ssst-sub-row">
		<small><?=$this->lang->line('media_library')?></small>
		<h3><a href="<?=base_url($categories[$media_library[0]["category_id"]]['url'].'/'.$media_library[0]["url"])?>"><?=word_limiter($media_library[0]["title"], 6)?></a></h3>
		<p class="adjust">
			<?php
			if ($media_library[0]["introtext"] == "")
				echo word_limiter(strip_tags($media_library[0]["fulltext"]),31);
			else
				echo word_limiter(strip_tags($media_library[0]["introtext"]),31);
			?>
		</p>
	</div>
</div>

<div class="content row clearfix ssst-row">
	<div class="pad col-md-10" style="padding: 10px 30px;">
		<h3 style="text-align: center;">
			<a href="<?=base_url('/static-page/medical-studies-in-english')?>">Medical Studies @ SSST
			</a>
		</h3>
		<br>
		<p style="text-align: justify;">
			<?php $s = 'In the 2014/2015 academic year the University Sarajevo School of Science and Technology will introduce a new twelve-semester (six-year) Medical Doctor Study Programme in English. Graduates will receive a Doctor of Medicine degree (MD). As the first private medical school of its kind in the region, SSST will utilise the existing facilities within the University’s main campus building. Its medical laboratories for the pre-clinical portion of the programme will be located in the subterranean level of the main campus building and will cover about 1800 square meters of space. The laboratories will have cutting-edge medical equipment and software which is even lacking at the state medical universities in the Balkan region. Students will have to complete their non-clinical training in order to continue on their studies and complete the clinical portion of their training at the Clinical Centre of Sarajevo (KCUS), the main clinical centres with which SSST has signed an agreement and entered into partnership. SSST has also signed agreements with other clinics, hospitals and emergency services in the country.'; ?>
			<?=word_limiter($s,100)?><br>
			<a href="<?=base_url('/static-page/medical-studies-in-english')?>"><?=$this->lang->line('read_more')?>...</a>
		</p>
		<!-- <div class="text-center">
			<ul class="pagination pagination-sm">
			  <li><a href="#">&laquo;</a></li>
			  <li class="active"><a href="#">1</a></li>
			  <li class="disabled"><a href="#">2</a></li>
			  <li><a href="#">3</a></li>
			  <li><a href="#">&raquo;</a></li>
			</ul>
		</div> -->
	</div>
	<div class="col-md-6 ssst-no-padding-left">
		<img src="images/medical.jpg" alt="" width="100%">
	</div>



</div>
