<?php $uri=$this->uri->segment(2); $language = $this->session->userdata('language'); ?>
<?php if ($language == 'bosnian') :?>
<header>
   <div class="row header">
      <div id="logo"> <a href="<?=base_url()?>"><img src="<?=base_url()?>images/logo.jpg" alt="" class="hidden-xs"></a> </div>
      <div id="logo"> <a href="<?=base_url()?>"><img src="<?=base_url()?>images/logo-xs.jpg" alt="" class="visible-xs" style="margin-bottom: 10px;"></a> </div>

      <div id="latin" class="hidden-xs hidden-sm"> <img src="<?=base_url()?>images/latin.jpg" alt="" class="hidden-xs hidden-sm"> </div>
      <div class="clearfix"></div>
      <div class="date-sentence hidden-xs hidden-sm"> <small><strong><?=date('l j. F Y.')?></strong> / Action is the foundational key to all success. (Pablo Picasso)</small> </div>

      <div class="tools col-xs-16 col-md-6 pull-right">
         <?=form_open('search')?>
         <?=form_input('search','','class="search" id="search" placeholder="Search"')?>
         <?=form_close()?>
         <div class="ssst-blue lang"> <a href="<?=base_url('/change_language/bosnian')?>">BIH</a> </div>
         <div class="ssst-blue lang"> <a href="<?=base_url('/change_language/english')?>">ENG</a> </div>
      </div>
   </div>
   <!-- Fixed navbar -->
   <div class="" style="">
    <div class="navbar navbar-default" role="navigation">
      <div class="">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" id="header-menu">
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  O SSST-u
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url()?>/static-page/upoznajte-ssst">Upoznajte SSST</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/poruka-rektora">Poruka Rektora</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/ssst-kampus">Servisi i usluge</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/menadzment-univerziteta">Menadžment univerziteta</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/partnerstvo-u-izvrsnosti">Univerzitet Buckingham</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/zasto-sarajevo">Zašto Sarajevo</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/kontakt">Kontakt</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Fakulteti
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url()?>/static-page/fakultet-kompjuterskih-nauka">Kompjuterske nauke</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/fakultet-informacionih-sistema">Informacioni sistemi</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/ekonomski-fakultet">Ekonomija</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/fakultet-politickih-nauka-i-medunarodnih-odnosa">Političke nauke i međunarodni odnosi</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/fakultet-stranih-jezika">Moderni jezici</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/sarajevska-filmska-akademija">Sarajevska filmska akademija</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/fakultet-inzenjerskih-nauka">Inžinjerske nauke</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/studij-medicine-na-engleskom-jeziku">Studij medicine</a> </li>
               </ul>
            </li>

            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Trenutni studenti
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">

                  <li> <a href="<?=base_url()?>/static-page/akademski-kalendar">Akademski kalendar</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/studentska-sluzba">Studentska služba</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/mogucnost-staziranja">Praksa</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/it-ured">IT ured</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/biblioteka">Biblioteka</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/studentske-aktivnosti-i-klubovi">Studentske aktivnosti</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/ssst-alumni">Alumni</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Budući studenti
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/studijski-programi')?>">Studijski programi</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/prijava-za-upis-na-ssst">Upis na SSST</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/prijemni-ispiti-2014-2015">Prijemni ispiti</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/finansiranje-vaseg-skolovanja-na-ssst-u">Školarina, studentski krediti i stipendije</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/dan-otvorenih-vrata">Dan otvorenih vrata</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Istraživanja
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url()?>/static-page/jacanje-kulture-istrazivanja-i-razvoja">O istraživačkom radu na SSST-u</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/istrazivacki-instituti">Istraživacki instituti</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/projekti-i-saradnja">Projekti i saradnja</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/publikacije">Novije publikacije</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/pocasne-katedre-i-doktorati">Počasne katedre i profesure</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/met-fondacija">MET fondacija</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" style="margin-right: 0px;" data-toggle="dropdown">
                  Vijesti i događaji
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">

                                    <li> <a href="<?=base_url()?>/news">Zadnje vijesti</a> </li>
                  <li> <a href="<?=base_url()?>/events">Događaji</a> </li>
                  <li> <a href="#">Obavijesti za medije</a> </li>
                  <li> <a href="<?=base_url()?>/media-library">Media Library</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/ssst-independent">SSST Independent</a> </li>
                  <li> <a href="<?=base_url()?>/static-page/pozivi-za-aplikacije-i-konkursi">Pozivi za aplikacije i konkursi</a> </li>

               </ul>
            </li>
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
    </div>
   </div>
</header>

<?php else : ?>

<header>
   <div class="header row">
      <div id="logo"> <a href="<?=base_url()?>"><img src="<?=base_url()?>images/logo.jpg" alt="" class="hidden-xs"></a> </div>
      <div id="logo"> <a href="<?=base_url()?>"><img src="<?=base_url()?>images/logo-xs.jpg" alt="" class="visible-xs" style="margin-bottom: 10px;"></a> </div>

      <div id="latin" class="hidden-xs hidden-sm"> <img src="<?=base_url()?>images/latin.jpg" alt=""> </div>
      <div class="clearfix"></div>
      <div class="date-sentence hidden-xs hidden-sm"> <small><strong><?=date('l j. F Y.')?></strong> / Action is the foundational key to all success. (Pablo Picasso)</small> </div>

      <div class="tools col-xs-16 col-md-6 pull-right">
         <?=form_open('search')?>
         <?=form_input('search','','class="search" id="search" placeholder="Search"')?>
         <?=form_close()?>         <div class="ssst-blue lang"> <a href="<?=base_url('/change_language/bosnian')?>">BIH</a> </div>
         <div class="ssst-blue lang"> <a href="<?=base_url('change_language/english')?>">ENG</a> </div>
      </div>
   </div>
   <!-- Fixed navbar -->
   <div class="" style="">
    <div class="navbar navbar-default" role="navigation">
      <div class="">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" id="header-menu">
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  About SSST
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/introducing-ssst')?>">Introducing SSST</a> </li>
                  <li> <a href="<?=base_url('/static-page/message-from-the-rector')?>">Message from the Rector</a> </li>
                  <li> <a href="<?=base_url('/static-page/ssst-campus')?>">Services and Facilities</a> </li>
                  <li> <a href="<?=base_url('/static-page/university-management')?>">University Management</a> </li>
                  <li> <a href="<?=base_url('/static-page/a-partnership-in-excellence')?>">University of Buckingham</a> </li>
                  <li> <a href="<?=base_url('/static-page/why-sarajevo')?>">Why Sarajevo</a> </li>
                  <li> <a href="<?=base_url('/static-page/contact-us')?>">Contact us</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Departments
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/computer-science-department')?>">Computer Science</a> </li>
                  <li> <a href="<?=base_url('/static-page/department-of-information-systems')?>">Information Systems</a> </li>
                  <li> <a href="<?=base_url('/static-page/department-of-economics')?>">Economics</a> </li>
                  <li> <a href="<?=base_url('/static-page/political-science-and-international-relations-department')?>">Political Science and International Relations</a> </li>
                  <li> <a href="<?=base_url('/static-page/modern-languages-department')?>">Modern Languages</a> </li>
                  <li> <a href="<?=base_url('/static-page/sarajevo-film-academy')?>">Sarajevo Film Academy</a> </li>
                  <li> <a href="<?=base_url('/static-page/department-of-engineering-science')?>">Engineering Sciences</a> </li>
                  <li> <a href="<?=base_url('/static-page/medical-studies-in-english')?>">Sarajevo Medical School</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Current Students
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/academic-calendar')?>">Academic Calendar</a> </li>
                  <li> <a href="<?=base_url('/static-page/student-services')?>">Student Services</a> </li>
                  <li> <a href="<?=base_url('/static-page/internship-opportunities')?>">Internships</a> </li>
                  <li> <a href="<?=base_url('/static-page/it-office')?>">IT Office</a> </li>
                  <li> <a href="<?=base_url('/static-page/library')?>">Library</a> </li>
                  <li> <a href="<?=base_url('/static-page/student-activities-and-clubs')?>">Student Activities and Clubs</a> </li>
                  <li> <a href="<?=base_url('/static-page/ssst-alumni')?>">Alumni</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Prospective Students
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/degree-programmes')?>">Degree Programmes</a> </li>
                  <li> <a href="<?=base_url('/static-page/applying-to-ssst')?>">Applying to SSST</a> </li>
                  <li> <a href="<?=base_url('/static-page/entrance-exams-2014-2015')?>">Entrance Exams</a> </li>
                  <li> <a href="<?=base_url('/static-page/financing-your-education-at-ssst')?>">Fees, Loans and Scholarships</a> </li>
                  <li> <a href="<?=base_url('/static-page/open-day')?>">Open Day</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Research
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/static-page/fostering-a-culture-of-research-and-development')?>">About Research at SSST</a> </li>
                  <li> <a href="<?=base_url('/static-page/research-institutes')?>">Research Institutes</a> </li>
                  <li> <a href="<?=base_url('/static-page/projects-and-collaborations')?>">Projects and Collaborations </a> </li>
                  <li> <a href="<?=base_url('/static-page/recent-publications')?>">Recent Publications</a> </li>
                  <li> <a href="<?=base_url('/static-page/honorary-chairs-and-professorships')?>">Honorary Chairs and Professorships</a> </li>
                  <li> <a href="<?=base_url('/static-page/met-foundation')?>">MET Foundation</a> </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" style="margin-right: 0px;" data-toggle="dropdown">
                  News and Events
               </a>
               <ul class="dropdown-menu sub-menu" role="menu">
                  <li> <a href="<?=base_url('/news')?>">Latest News</a> </li>
                  <li> <a href="<?=base_url('/events')?>">Events</a> </li>
                  <li> <a href="#">Press Releases</a> </li>
                  <li> <a href="<?=base_url('/media-library')?>">Media Library</a> </li>
                  <li> <a href="<?=base_url('/static-page/ssst-independent')?>">SSST Independent</a> </li>
                  <li> <a href="<?=base_url('/static-page/calls-for-applications-and-vacancies')?>">Calls for Applications and Vacancies</a> </li>
               </ul>
            </li>

          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
    </div>
   </div>
</header>
<?php endif; ?> <?php /*<li <?=($uri == 'showtimes-dvd') ? 'class="active"' : ''?>><a href="/static-page/release-dates">&bull; RELEASE DATES</a></li> */ ?>
