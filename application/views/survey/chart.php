<?php $this->load->view('survey/header'); ?>
<hr class="reset" />
<?php 
    // var_dump($p); 
    //var_dump($subjectData);
    // $d = $p;
?>    
 
<?php 

$colors =array('#8B0000', '#B22222', '#FFD700', '#98FB98', '#2E8B57'); 
// http://colours.neilorangepeel.com/

?>


<style>
.pie-legend{list-style-type: none;}    
.pie-legend span{
    position: relative;
    
    padding: .4em .4em .4em 1.2em;
    *padding: .4em;
    margin: 0em 1em 0em .5em;
    background: #ddd;
    color: #444;
    text-decoration: none;
    transition: all .3s ease-out;   
}


.pie-legend span:after{
    position: absolute; 
    content: '';
    border: .5em solid transparent;
    left: -3em;
    top: 50%;
    margin-top: 3em;
    margin: .5em 0 .5em .5em;
    transition: all .3s ease-out;               
}



</style>

<h1>Charts</h1> <br>
<hr class="reset">

    <h3>Subject: <?=$subjectData[0]->code; ?> - <?=$subjectData[0]->subject; ?>  </h3>
    <div>
        <!-- Code: <?=$subjectData[0]->code; ?> <br>
        Subject: <?=$subjectData[0]->subject; ?>  <br> -->
            
        <b>

        Professor: <?=$subjectData[0]->professor; ?>  <br>
        Year: <?=$subjectData[0]->year; ?>  <br>
        Tokens used: <?=$subjectData[0]->usedTokens; ?>  <br>

        </b>
        
    </div>

<hr class="reset">

<?php 
if ($subjectData[0]->usedTokens == 0) {
    echo "<h3>No tokens entered for this survey!</h3>";
}else{
//$count = count($p); 
$i = 0;

foreach ($p as $p1 => $v):?>


    <h3>Question - <?=$v['res']['pitanje']?></h3>

    <br>

    <div style="width:50%; height:250px; float:left;">
    <canvas id="myChart<?=$i?>" width="200" height="200" style="margin:auto;"></canvas>

    </div>

    <!-- Total answers:<?=$v['res']['total']?> <br>  -->
    Answers values and numbers: <br>
    <div id="legendDiv<?=$i?>" style="width:50%; float:right;"></div>

    <div class="clearfix"></div>

<?php $i++; endforeach; ?>

<div>
<h3>Comments: </h3>

    <table>
        <thead>
            <tr>
                <td style="width:50%; border-right:1px solid #E9E9E9; vertical-align:top;"><h5>What should be changed about the course, and how?</h5></td>
                <td style="width:50%;"><h5>What is good about the course?</h5></td>
            </tr>
        </thead>

        <tbody>
            
            <?php foreach ($comments as $comment => $value): ?>

            <tr>
                <td style="border-right:1px solid #E9E9E9; vertical-align:top; text-align:justify;"><?php echo $value->reply_10; ?></td>
                <td style="border-right:1px solid #E9E9E9; vertical-align:top; text-align:justify;"><?php echo $value->reply_11; ?></td>
            </tr>

            <?php endforeach; ?>

        </tbody>
        

    </table>


</div>

<hr class="clearfix">

<?php //var_dump($p); ?>

<script>
        var options = [{
        // Boolean - Whether to show labels on the scale
        scaleShowLabels: false,
        showTooltips: true,
        segmentStrokeWidth : 3,
        //String - A legend template
        legendTemplate : "Ocjene***: <ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\">|||||</span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        // legendTemplate : "1,3,5"


    }];
<?php 

//$count = count($p); 

$j = 0;
foreach ($p as $p1 => $v):?>

  var data<?=$j?> = [

    <?php $i = 0; foreach ($v['res']['items'] as $item => $value): ?>


        {
            value: <?=$value['percent']?>,
            color:"<?=$colors[$i]?>",
            highlight: "#008B8B",
            label: "<?=$value['grade']?>, Votes: <?=$value['count']?>",
            legendTemplate : "Ocjene: <ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\">|||||</span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

        },
        <?php $i++; endforeach; ?>

];

var ctx<?=$j?> = document.getElementById("myChart<?=$j?>").getContext("2d");
        // var ctx1 = document.getElementById("myChart1").getContext("2d");
        // var ctx2 = document.getElementById("myChart2").getContext("2d");
        var myLineChart<?=$j?> = new Chart(ctx<?=$j?>).Pie(data<?=$j?>, {
            animateScale: true,
            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value.toFixed(2) %>",


        });

        //var myLineChart1 = new Chart(ctx1).Pie(data, options);
        // var myLineChart = new Chart(ctx2).Pie(data);
        document.getElementById("legendDiv<?=$j?>").innerHTML = myLineChart<?=$j?>.generateLegend(options);

<?php 
$j++;
endforeach; }?>

 


    


    


</script>

<?php $this->load->view('survey/footer'); ?>