<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><?=$title = (isset($page_title)) ? $page_title : "SSST survey" ; ?></title>
            <!-- META INFO -->
            <meta name="description" content="Konkurs za dodjelu stipendija udruženja “Spajalica”, za akademsku 2013/14. godinu - PRIJAVNI OBRAZAC">
            <meta name="keywords" content="Udruzenje Spajalica, Sarajevo" />
            <meta name="author" content="Udruženje Spajalica" />

            <!-- LINK ICONS -->
            <link rel="shortcut icon" href="<?=base_url()?>images/spajalica.ico">
            <link rel="apple-touch-icon" href="<?=base_url()?>images/spajalica-apple-touch-icon.png">

            <!-- STYLESHEETS -->
            <link rel="stylesheet" href="/css/konkursi-styles.css">
            <link rel="stylesheet" href="/css/fonts.css?v=1">

            <!-- Jquery UI CSS  -->
            <link rel="stylesheet" type="text/css"  href="<?=base_url()?>css/jquery-ui-custom-theme-konkursi/jquery-ui-1.9.0.custom.css" />

            <!-- SCRIPTS -->
            <script src="/scripts/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="/scripts/jquery.meio.mask.min.js" charset="utf-8"></script>
            <script type="text/javascript" src="/scripts/jquery.numeric.js"></script>

            <!-- Load jQuery UI -->
            <script type="text/javascript" src="<?=base_url()?>scripts/jquery-ui-1.9.0.custom-konkursi.js"></script>
            
            <!-- DataTables CSS -->
            <link rel="stylesheet" type="text/css" href="<?=base_url()?>scripts/dtables/dataTables.tableTools.css">
            <link rel="stylesheet" type="text/css" href="<?=base_url()?>scripts/dtables/jquery.dataTables.css">
              
            <!-- jQuery
            <script type="text/javascript" charset="utf8" src="<?=base_url()?>scripts/dtables/jquery.js"></script>   
              
            <!-- DataTables -->
            <script type="text/javascript" charset="utf8" src="<?=base_url()?>scripts/dtables/jquery.dataTables.js"></script>
            <script type="text/javascript" charset="utf8" src="<?=base_url()?>scripts/dtables/dataTables.tableTools.js"></script>
            

            <!-- Charts.js -->
            <script src="<?=base_url()?>scripts/chart.js"></script>

            <style type="text/css">
                    /* form correction */
                    .error {
                        color: #990000;
                        margin: 30px 0;
                        text-align: center;
                        border: 1px dashed #990000;
                        font-size: 30px;
                        font-weight: bold;
                    }
            </style>
            <script>
                jQuery(document).ready(function($){
                      $.mask.masks.telefon = {mask: '99999999999'}
                      $.mask.masks.mobitel = {mask: '999999999999'}
                      $.mask.masks.prosjek = {mask: '9.99'}
                      $.mask.masks.datum = {mask: '39.19.9999'}
                      $.mask.masks.captcha = {mask: '9999'}
                      $('.telefon').setMask();
                      $('.mobitel').setMask();
                      $('.prosjek').setMask();
                      $('.datum_rodjenja').setMask();
                      $('.datum').setMask();
                      $('.captcha').setMask();
                      $(".numeric").numeric();

                        // Datepicker
                      $('.datepicker').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            defaultDate: new Date(1989, 00, 01),
                            dateFormat: "dd.mm.yy"
                      });
               });
        </script>
    </head>
<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
    <body>
<!--<![endif]-->
        <div id="wrapper" class="grid1 clearfix" style="width: 700px; background-image: url('/images/konkurs-wrapper.gif');"" >
            <hr class="top" />
            <!--[if lt IE 7 ]><br /><![endif]-->
            <!--[if IE 7 ]><br /><![endif]-->
            <!--[if IE 8 ]><br /><![endif]-->
            <header style="display: block;">
                    <div style="">
                        <table border="0" cellpadding="0" cellspacing="0" style="">
                            <tr valign="bottom">
                                <td style="border: none; padding: 9px; width: 72px">
                                    <a href="http://ssst.edu.ba/" style=" " target="_blank"><img src="http://ssst.edu.ba/images/logo.jpg" border="0" alt="Logo" style="" width="300"></a>
                                </td>
                                 <td style="border: none; padding: 0 0 0 10px; font-weight: bold; vertical-align: bottom; width: 100px;">
                                    
                                </td>
                                <td style="border: none; padding: 25px 0 0 0; text-align: right;">
                                    <h3 style="padding: 0;">SSST Survey 2014/15 - fall semseter</h3>
                                </td>
                            </tr>
                        </table>
                    </div>
            </header>
<script>

  $(document).ready(function() {
    $('#example').DataTable({
      scrollY: 1100,
      paging: false,
      "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "<?=base_url(); ?>scripts/dtables/swf/copy_csv_xls_pdf.swf"
        }

  });
} );

</script>

