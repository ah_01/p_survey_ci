            
            <!-- FOOTER -->
            <footer>
              <hr />
              <div class="left">© Copyright <a href="http://ssst.edu.ba" target="_blank" style="color: #666; text-decoration: none;">SSST 2014</div>
                <div class="right">
                      <a href="mailto:it@ssst.edu.ba" target="_blank" style="color: #666; text-decoration: none;">Sarajevo School of Science and Technology - IT Department</a>
                </div>
            </footer>
            <!--[if lt IE 7 ]><br /><br /><br /><![endif]-->
            <!--[if IE 7 ]><br /><br /><br /><![endif]-->
            <!--[if IE 8 ]><br /><br /><br /><![endif]-->
            <hr class="bottom"/>
            <!-- /FOOTER -->

        </div>
        <!-- google analytics -->
        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-5578635-2']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

        </script>
    </body>
</html>
