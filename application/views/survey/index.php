<?php $this->load->view('survey/header'); ?>
<hr class="reset" />

		<?php
            // $this->session->flashdata('message');
            $fmessage = $this->session->flashdata('message');
            // var_dump($fmessage);
            if ($fmessage == true)
            {
                echo '<div class="row error"><h4>'.$fmessage.'</h4></div>';
            }

			if (isset($error_message))
			{
				echo '<div class="row error">'.$error_message.'</div>';
			}

		?>

        <div class="row" style="margin:40px 0; padding: 10px 0px; text-align: center;">
     
            <?=form_open(base_url('survey/forma'))?>
            <?=form_label("Enter token","token",array('style'=>'display: inline; font-weight: bold; font-size: 18px;'));?> &nbsp;&nbsp;&nbsp;   
            <?=form_input('token',set_value('token'),'class="box4" style="display: inline; width: 100px; text-decoration: uppercase; text-transform:uppercase;" maxlength="6"'); ?>&nbsp;&nbsp;&nbsp; 
            <?=form_submit('submit','SUBMIT','class="submit" style="text-align: center; width: 130px; padding: 5px 10px;"')?>
            <?=form_close()?>
            
        </div>

<?php $this->load->view('survey/footer'); ?>