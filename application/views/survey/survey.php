<?php $this->load->view('survey/header'); ?>
<hr class="reset" />
  <?php 
  $msg = (isset($message)) ? $message : " " ;
  echo $msg;

   ?>
   <?=form_open('survey/complete')?>

            <h2 style="color: #990000;" class="text-center">SSST STUDENT SURVEY FOLLOW‐UP </h2>
            <h6 class="text-center"><small style="color: #4585E6;">TO BE COMPLETED IN THE SAME WAY AS SURVEY 1 – EVERY COURSE SURVEYED.</small></h6>
            <br>
            <h6>Survey for subject: <?php echo $subject->code . " - " . $subject->subject; ?></h6>
            <h6>Professor: <?php echo $subject->professor; ?></h6>
            <h6>Token used: <?=$token; ?></h6>
            <br>
            <br>

            <h4 class="text-center"><small style="color: #4585E6;">Note: 1-lowest value, 5-highest value</small></h6>
         
                    
            <?php
            echo form_hidden('subject_id', $subject->id);
            echo form_hidden('token', $token);


            ?>

          <div class="row">
             <?php echo validation_errors(); ?>
          </div>

          <div class="form-group" id="condition">
            <?=form_label("1. The class is well organised?", "q1"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q1'.'" value="'.$i.'" '. set_radio('q1', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>


          <div class="form-group" id="condition">
            <?=form_label("2. I know what is expected of me in this class?", "q2"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q2'.'" value="'.$i.'" '. set_radio('q2', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>


          <div class="form-group" id="condition">
            <?=form_label("3. The instructor seems well prepared for class.?", "q3"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q3'.'" value="'.$i.'" '. set_radio('q3', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>


          <div class="form-group" id="condition">
            <?=form_label("4. The instructor explains clearly?", "q4"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q4'.'" value="'.$i.'" '. set_radio('q4', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>


          <div class="form-group" id="condition">
            <?=form_label("5. There is sufficient time in class for questions/discussions during tutorials/labs", "q5"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q5'.'" value="'.$i.'" '. set_radio('q5', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>

          <div class="form-group" id="condition">
            <?=form_label("6. Course assignments, homework and quizzes are useful components of this course?", "q6"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q6'.'" value="'.$i.'" '. set_radio('q6', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>

          <div class="form-group" id="condition">
            <?=form_label("7. The instructor is available for consultation outside of class?", "q7"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q7'.'" value="'.$i.'" '. set_radio('q7', $i).'/>&nbsp;'.$i; } ?>
          </div>

        <hr>
          <div class="form-group" id="condition">
            <?=form_label("8. In this class I am treated equitably and with respect?", "q8"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q8'.'" value="'.$i.'" '. set_radio('q8', $i).'/>&nbsp;'.$i; } ?>
          </div>

        <hr>
          <div class="form-group" id="condition">
            <?=form_label("9. The instructor is a good teacher, overall?", "q9"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q9'.'" value="'.$i.'" '. set_radio('q9', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>

          <div class="form-group">

            <?=form_label("10. What should be changed about the course, and how??", "q10"); ?> <br>
            <div>
            <?php 
                $data = array( 'name' => 'q10', 'id' => 'q10', 'class' => 'form-control', 'rows' => 3, );
                echo form_textarea($data);
            ?>            

            </div>
          </div>
<hr>

          <div class="form-group">

            <?=form_label("11. What is good about the course?", "q11"); ?> <br>
            <div>
            <?php 
            $data = array( 'name' => 'q11', 'id' => 'q11', 'class' => 'form-control', 'rows' => 3, );
                echo form_textarea($data);
            ?>            

            </div>
          </div>

<hr>

          <div class="form-group" id="condition">
            <?=form_label("12. The course textbook/handouts are appropriate and useful to the course?", "q12"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q12'.'" value="'.$i.'" '. set_radio('q12', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>

          <div class="form-group" id="condition">
            <?=form_label("13. Research resources provided to students, including online encyclopedia and journals, are useful?", "q13"); ?> <br>
            <?php for ($i=1; $i < 6 ; $i++) { echo '<input type="radio" name="q13'.'" value="'.$i.'" '. set_radio('q13', $i).'/>&nbsp;'.$i; } ?>
          </div>
        <hr>

            







         <div style="text-align:right; margin:25px 0;">
             
              <?=form_submit('submit', 'Submit!', 'class="submit" style="text-align: center; width: 130px; padding: 5px 10px;"');?>

         </div>
          

<?=form_close();?>

<?php $this->load->view('survey/footer'); ?>