<?php $this->load->view('survey/header'); ?>
<hr class="reset" />
 
<?php 

	$show = (isset($id)) ? true : false ;
	//var_dump($show);

?>


<?php if($show): ?>
	<h1>Token List </h1>
    <h3><?=$subject[0]['code'] . "-" . $subject[0]['subject'] . ", " .  $subject[0]['professor'];  ?> </h3>
    <?=anchor(base_url('/survey/lista'), "Back"); ?>
    
    
     <br>
<hr class="reset" />
	

<!-- <ol>
	<?php foreach ($lista as $subject): ?>
		  
			<li>Token - <?=$subject['token']; ?> | Status:   <?=$status = ($subject['status'] == 1) ? "Iskoristen" : "Neiskoristen" ; ?> </li>
 
		 
	<?php endforeach; ?>
 </ol>
 -->

<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
               
                <th>Id</th>
                <th>Token</th>
                <th>Status</th>

                
            </tr>
        </thead>
 			
        <tfoot>
            <tr>
                
  				<th>Id</th>
                <th>Token</th>
                <th>Status</th>

            </tr>
        </tfoot>


        <tbodyd>

 			<?php foreach ($aaData as $item): ?>
		        <tr>
	                <td><?=$item['id']  ?></td>
	                <td><?=$item['token']  ?></td>
	                <td><?=$status = ($item['status']==1) ? "1-Iskoristen" : "0-neiskoristen" ;  ?></td>

	                
	            </tr>
			<?php endforeach; ?>


        </tbodyd>
    </table>




<?php else: ?>	

	
	<h1>Survey List</h1>

<!-- 	<?php foreach ($lista as $subject): ?>
		  <li>
			<? $subject['code'];?>  <? $subject['subject'];?>  
		    <?=anchor( base_url("survey/tispis/".$subject['id']), $subject['code'] . "-" . $subject['subject']);?><br>


		  </li>
	<?php endforeach; ?> -->


	<br>
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
               
                <th>Code</th>
                <th>Subject</th>
                <th>Professor</th>
                <th>Year</th>
                <th>Department</th>
                <th>Used Tokens</th>
                <th>Report</th>
                
            </tr>
        </thead>
            
        <tfoot>
            <tr>
                
                <th>Code</th>
                <th>Subject</th>
                <th>Professor</th>
                <th>Year</th>
                <th>Department</th>
                <th>Used Tokens</th>
                <th>Report</th>

            </tr>
        </tfoot>


        <tbodyd>

 			<?php foreach ($aaData as $item): ?>
		        <tr>
	                <td><?=$item['code']  ?></td>
	                <td><?=anchor( base_url("survey/tispis/".$item['id']), $item['subject']);?></td>
	                <td><?=$item['professor']  ?></td>
	                <td><?=$item['year']  ?></td>
                    <td><?=$item['department']  ?></td>
                    <td><?=$item['tokensUsed']  ?></td>
	                <td><?=anchor( base_url("survey/report/".$item['id']), "Report");?></td>
	                
	            </tr>
			<?php endforeach; ?>


        </tbodyd>
    </table>



<?php endif; ?>	

<?php $this->load->view('survey/footer'); ?>