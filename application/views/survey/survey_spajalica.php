<?php $this->load->view('konkursi/header'); ?>
<hr class="reset" />
<div class="row" style="padding: 10px 0px; text-align: center;">
        <h1 style="margin-bottom: 5px; color: #333;">PRIJAVNI OBRAZAC</h1>
        za dodjelu deset (11) stipendija Udruženja "Spajalica" u akademskoj 2014/15.
        godini redovnim studentima druge ili viših godina (I) prvog ciklusa studija
        Univerziteta u Sarajevu po Bolonjskom procesu.<br />
        Tekst konkursa možete pogledati <a href="http://www.stipendije.ba/stipendija/1164" target="_blank">ovdje</a>.
</div>
<hr class="reset" />
<div class="row" style="padding: 10px 0px;">
    Prije samog pristupa online prijavnom obrascu obavezno pročitajte konkurs. Studenti koji ispunjavaju uslove konkursa trebaju detaljno popuniti online prijavni obrazac. Svi podaci koje unosite prilikom apliciranja moraju biti tačni i isključivo vezani za vas, jer ćete sve navedeno naknadno morati dokazati validnim dokumentima, u protivnom gubite pravo na stipendiju.
</div>

<hr class="reset" />
<?php if (validation_errors() != "") : ?> 
<div class="row" style="padding: 10px 0px;">
      <div class="form-status">
              <div class="error"><?=validation_errors()?></div>
        </div>
    <hr />
     <?php elseif (isset($thankyou) != '') : // vea fix - GROZNA provjera ?>
    <hr />
      <div class="form-status">
        <span style="color: #990000;"><?php echo set_value('name'); ?></span>, Vaša poruka je uspješno poslana.<br />
        </div>
    <hr />
</div>
<?php endif; ?>

<?=form_open('/konkursi/submit_konkurs','',Array('konkurs_id'=>'2'))?>
<div class="row" style="padding: 10px 0px;">
    <h2>KONTAKT PODACI</h2>
            <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 25%;" valign="middle"><?=form_label("Ime: ","ime");?></td>
                    <td style="width: 35%;" valign="middle"><?=form_input('ime',set_value('ime'),'class="box4"'); ?></td>
                    <td style="width: 40%;" valign="middle"></td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Ime jednog roditelja: ","ime_roditelja");?></td>
                    <td valign="middle"><?=form_input('ime_roditelja',set_value('ime_roditelja'),'class="box4"'); ?></td>
                    <td valign="middle"></td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Prezime: ","prezime");?></td>
                    <td valign="middle"><?=form_input('prezime',set_value('prezime'),'class="box4"'); ?></td>
                    <td valign="middle"></td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Telefon: ","telefon");?></td>
                    <td valign="middle"><?=form_input('telefon',set_value('telefon'),'class="box4 telefon" id="telefon" alt="telefon" style="text-align: right;"'); ?></td>
                    <td valign="middle"><strong>(*)</strong></td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Mobitel: ","mobitel");?></td>
                    <td valign="middle"><?=form_input('mobitel',set_value('mobitel'),'class="box4 mobitel" id="mobitel" alt="mobitel" style="text-align: right;"'); ?></td>
                    <td valign="middle"><strong>(*)</strong> OBAVEZNO aktivan broj radi SMS provjere!</td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Email: ","email");?></td>
                    <td valign="middle"><?=form_input('email',set_value('email'),'class="box4"'); ?></td>
                    <td valign="middle">OBAVEZNO validna email adresa radi potvrde!</td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Adresa: ","adresa");?></td>
                    <td valign="middle"><?=form_input('adresa',set_value('adresa'),'class="box4"'); ?></td>
                    <td valign="middle">Ulica, poštanski broj i grad.</td>
                </tr>
                <tr>
                    <td valign="middle"><?=form_label("Općina / grad / distrikt:","opstina");?></td>
                    <td valign="middle">
                            <?php
                                $options = get_array_subnode($this->config->item('opstine'),'naziv');
                                setlocale(LC_ALL,'hr_HR');
                                asort($options,SORT_LOCALE_STRING);
                                echo form_dropdown('opstina', $options, set_value('opstina'),'class="box5" style="width: 99%;"');
                            ?>
                    </td>
                    <td valign="middle"></td>
                </tr>
            </table>
</div>

<strong>(*)</strong> Brojevi telefona treba da budu u formatu: <strong>38761123456</strong>. Bez predznaka "<strong>+</strong>" ili "<strong>00</strong>" na samom početku!

<hr />
<div class="row" style="padding: 10px 0px;">
    <h2>1.0. Podaci o školovanju</h2>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("1.1. Da li ste student Univerziteta u Sarajevu?","univerzitet_sarajevo");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('univerzitet_sarajevo', $options, set_value('univerzitet_sarajevo'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("1.2. Da li ste prvi put upisani kao redovan, <strong>(ne paralelan ili samofinansirajuci)</strong> student u ak. 2014/15. godinu?","redovan");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('redovan', $options, set_value('redovan'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("1.3. Da li možete dostaviti uvjerenje o redovnom školovanju?","uvjerenje_redovno_skolovanje");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('uvjerenje_redovno_skolovanje', $options, set_value('uvjerenje_redovno_skolovanje'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("1.4.  Da li možete dostaviti uvjerenje o položenim ispitima sa prosjekom ocjena?","uvjerenje_polozeni_ispiti");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('uvjerenje_polozeni_ispiti', $options, set_value('uvjerenje_polozeni_ispiti'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
  <hr />
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;" >
            <tr>
                <td style="width: 30%;" valign="middle"><?=form_label("1.5. Fakultet na kojem studirate","fakultet");?></td>
                <td style="width: 70%;" valign="middle">
                        <?php
                            $options = $this->config->item('fakulteti');
                            echo form_dropdown('fakultet', $options, set_value('fakultet'),'class="box5" style="width:100%;"');
                        ?>
                </td>
            </tr>
    </table>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td style="width: 30%;" valign="middle"><?=form_label("1.6. Trajanje studija","trajanje_studija");?></td>
                <td style="width: 25%;" valign="middle">
                        <?php
                            $options = Array(''=>'','3'=>'3 godine', '4'=>'4 godine','5'=>'5 godina','6'=>'6 godina');
                            echo form_dropdown('trajanje_studija', $options, set_value('trajanje_studija'),'class="box5"');
                        ?>
                </td>
                <td style="width: 45%;" >Konkurs je samo za prvi ciklus studija!</td>
            </tr>
            <tr>
                <td style="width: 30%;" valign="middle"><?=form_label("1.7. Godina studija","godina_studija");?></td>
                <td style="width: 25%;" valign="middle">
                        <?php
                            $options = Array(''=>'','2'=>'2. godina','3'=>'3. godina','4'=>'4. godina','5'=>'5. godina','6'=>'6. godina');
                            echo form_dropdown('godina_studija', $options, set_value('godina_studija'),'class="box5"');
                        ?>
                </td>
                <td style="width: 45%;" ></td>
            </tr>
            <tr>
                <td valign="middle" style="width: 30%;" ><?=form_label("1.8. Prosjek ocjena","prosjek_ocjena");?></td>
                <td valign="middle" style="width: 25%;" ><?=form_input('prosjek_ocjena',set_value('prosjek_ocjena'),'class="box4 prosjek" style="width: 94%; text-align: right;" id="prosjek" alt="prosjek" '); ?></td>
                <td valign="middle" style="width: 45%;" ><strong>(**)</strong></td>
            </tr>
    </table>
</div>

<strong>(**) Unijeti prosjek sa dvije decimale! (Nije potrebno kucati zarez ili tačku)</strong>

<hr />
<div class="row" style="padding: 10px 0px;">
    <h2>2.0. Podaci o drugim stipendijama</h2>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;">
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("2.1. Da li primate stipendiju ili novčanu naknadu od nekog drugog pravnog ili fizičkog lica? ","prima_stipendiju");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('prima_stipendiju', $options, set_value('prima_stipendiju'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("2.2. Da li možete dostaviti izjavu da ne primate stipendiju ili novčanu naknadu od nekog drugog pravnog ili fizičkog lica?","izjava_o_neprimanju_stipendije");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('izjava_o_neprimanju_stipendije', $options, set_value('izjava_o_neprimanju_stipendije'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
</div>

<hr />
<div class="row" style="padding: 10px 0px;">
    <h2>3.0. Podaci o aplikantu/studentu</h2>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("3.1. Da li ste državljanin BiH i imate li stalno mjesto prebivalište u BiH?","stalno_prebivaliste");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('stalno_prebivaliste', $options, set_value('stalno_prebivaliste'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("3.2. Da li možete dostaviti dvije originalne fotografije 3,5 x 4,5 cm?","fotografije");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('fotografije', $options, set_value('fotografije'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("3.3. Da li je na ovaj konkurs aplicirao i Vaš brat ili sestra?","samo_jedna_prijava");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('samo_jedna_prijava', $options, set_value('samo_jedna_prijava'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
    <hr />
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;">
<!--        <tr>
            <td style="width: 60%;" valign="middle"><?=form_label("3.4. Unijeti Vaš Jedinstveni Matični Broj Građana (JMBG)","jmbg");?></td>
            <td style="width: 40%;" valign="middle"><?=form_input('jmbg',set_value('jmbg'),'class="box4 numeric" maxlength="13" style="text-align: right;" '); ?></td>
        </tr>
        <tr>
            <td valign="middle"><?=form_label("3.5. Unijeti broj lične karte","broj_licne_karte");?></td>
            <td valign="middle"><?=form_input('broj_licne_karte',set_value('broj_licne_karte'),'class="box4" maxlength="9" style="text-align: right; text-transform: uppercase;" '); ?></td>
        </tr>-->
        <tr>
            <td valign="middle"><?=form_label("3.4. Datum rođenja <strong>(****)</strong>","datum_rodjenja");?></td>
            <td valign="middle"><?=form_input('datum_rodjenja',set_value('datum_rodjenja'),'class="box4 datum datepicker" alt="datum" style="text-align: right; text-transform: uppercase;" '); ?></td>
        </tr>
        <tr>
            <td style="width: 60%;" valign="middle"><?=form_label("3.5. Mjesto rođenja","mjesto_rodjenja");?></td>
            <td style="width: 40%;" valign="middle"><?=form_input('mjesto_rodjenja',set_value('mjesto_rodjenja'),'class="box4"'); ?></td>
        </tr>
    </table>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;" >
        <tr>
            <td style="width: 85%;" valign="middle"><?=form_label("3.6. Spol/Sex/Gender","gender");?></td>
            <td style="width: 15%;" valign="middle">
                    <?php
                            $options = Array(''=>'', '1'=>'MUŠKO','0'=>'ŽENSKO');
                            echo form_dropdown('gender', $options, set_value('gender'),'class="box5"');
                    ?>
            </td>
        </tr>
    </table>
</div>
<strong>(****)</strong> Datum rođenja u formatu <strong>dd.mm.gggg</strong>. (Nije potrebno kucati tačku "<strong>.</strong>")

<hr />
<div class="row" style="padding: 10px 0px;">
    <h2>4.0. Podaci o užim članovima porodice i domaćinstva:</h2>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("4.1. Da li možete dostaviti uvjerenje o zajedničkom domaćinstvu?","uvjerenje_domacinstvo");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('uvjerenje_domacinstvo', $options, set_value('uvjerenje_domacinstvo'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("4.2. Da li možete dostaviti dokaz o mjesečnim primanjima  za svakog člana pojedinačno ili dokaz da nema primanja?","dokaz_o_primanjima");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('dokaz_o_primanjima', $options, set_value('dokaz_o_primanjima'),'class="box5"');
                            ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("4.3. Ako je neko od užih članova porodice RVI, civilna žrtva rata ili poginuo, da li možete dostaviti dokaz za to?","dokaz_o_rvi");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('dokaz_o_rvi', $options, set_value('dokaz_o_rvi'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
  <hr />
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;">
                <tr>
                    <td style="width: 75%;" valign="middle"><?=form_label("4.4. Ukupna primanja svih članova domaćinstva (KM):","ukupna_primanja");?></td>
                    <td style="width: 25%;" valign="middle"><?=form_input('ukupna_primanja',set_value('ukupna_primanja'),'class="box4 numeric" style="text-align: right;" '); ?></td>
                </tr>
    </table>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("4.5. Ukupan broj članova domaćinstva:","ukupno_clanova");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'');
                                for ($i = 1; $i <=10; $i++) { $options[$i] = $i; }
                                echo form_dropdown('ukupno_clanova', $options, set_value('ukupno_clanova'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 75%;" valign="middle"><?=form_label("4.6. Status roditelja:","roditelji");?></td>
                    <td style="width: 25%;" valign="middle">
                            <?php
                                $options = Array(''=>'','1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
                                echo form_dropdown('roditelji', $options, set_value('roditelji'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td style="width: 90%;" valign="middle"><?=form_label("4.7. Da li je neki drugi od užih članova Vaše porodice RVI?:","porodica_rvi");?></td>
                    <td style="width: 10%;" valign="middle">
                            <?php
                                $options = Array(''=>'', '1'=>'DA','0'=>'NE');
                                echo form_dropdown('porodica_rvi', $options, set_value('porodica_rvi'),'class="box5"');
                            ?>
                    </td>
                </tr>
    </table>
</div>

<div class="row" style="padding: 10px 0px 0px;">
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" >
        <tr valign="top">
                    <td style="width: 50%; vertical-align: top;"><?=form_label("Ukoliko imate neki komentar ili pitanje, molimo upišite ga u naredno polje.","komentar");?></td>
                    <td style="width: 50%;" valign="middle"><?php echo form_textarea('komentar',set_value('komentar'),'style="width: 97%;" class="box4"'); ?></td>
                </tr>
    </table>
</div>

<hr />
<div class="row" style="padding: 10px 0px 0px;">
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 1px;" >
                <tr>
                    <td style="width: 50%;" valign="middle"><?=form_label("Unijeti broj sa slike ispod: ","captcha")?></td>
                    <td style="width: 50%;" valign="middle"><?=form_input('captcha',set_value('captcha'),'class="box4 numeric captcha"')?></td>
                </tr>
    </table>
    <div style="padding: 10px 0 0 540px;">
        <?=$captcha['image']?>
    </div>
</div>

<hr />
<div class="row" style="padding: 10px 0px; text-align: right;">
    <?=form_reset('reset','RESET','class="submit" style="width:14%;"')?>
    <?=form_submit('submit','POŠALJI APLIKACIJU','class="submit" style="width:24%;"')?>
</div>

<?php echo form_close(); ?>

<?php $this->load->view('konkursi/footer'); ?>