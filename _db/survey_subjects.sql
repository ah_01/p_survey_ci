-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2014 at 02:08 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ssstedu_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `survey_subjects`
--

CREATE TABLE IF NOT EXISTS `survey_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `professor` varchar(200) DEFAULT NULL,
  `year` varchar(5) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=116 ;

--
-- Dumping data for table `survey_subjects`
--

INSERT INTO `survey_subjects` (`id`, `code`, `subject`, `professor`, `year`, `department`) VALUES
(1, 'CSIS110', 'Programming and Problem Solving I', 'Zeljko Juric', 'I', 'CSIS'),
(2, 'CSIS120', 'Introduction to Computer and Information Systems', 'Emir Ganic', 'I', 'CSIS'),
(3, 'MATH150', 'Calculus', 'Saida Sultanic', 'I', 'CSIS'),
(4, 'Lang121E', 'English Language - CSIS', 'Anida Hadzic Sabina Gadzo-Mutapcic', 'I', 'CSIS'),
(5, 'Lang121G', 'German Language Edin Konjhodzic - CSIS', 'Edin Konjhodzic ', 'I', 'CSIS'),
(6, 'Lang121G', 'German Language Nejra Ljubovic - CSIS', 'Nejra Ljubovic ', 'I', 'CSIS'),
(7, 'Lang121G', 'German Language Dzana Zahirovic - CSIS', 'Dzana Zahirovic', 'I', 'CSIS'),
(8, 'CSIS270', 'Design and Implementation in Web Environments', 'Vedad Hulusic', 'II', 'CSIS'),
(9, 'CSIS240', 'Database Systems', 'Amer Hadzikadic', 'II', 'CSIS'),
(10, 'MATH250', 'Selected Topics in Mathematics', 'Zeljko Juric', 'II', 'CSIS'),
(11, 'Lang221E', 'English Language - CSIS', 'Sabina Gadzo-Mutapcic', 'II', 'CSIS'),
(12, 'Lang221G', 'German Language Edin Konjhodzic - CSIS', 'Edin Konjhodzic ', 'II', 'CSIS'),
(13, 'Lang221G', 'German Language Nejra Ljubovic - CSIS', 'Nejra Ljubovic ', 'II', 'CSIS'),
(14, 'Lang221G', 'German Language Dzana Zahirovic - CSIS', 'Dzana Zahirovic', 'II', 'CSIS'),
(15, 'IS395', 'Strategic Information Systems', 'Anel Tanovic', 'III', 'CSIS'),
(16, 'IS385', 'Information Systems Security', 'Kemal Taljanovic', 'III', 'CSIS'),
(17, 'CS350', 'Data Structures', 'Dzejla Medjedovic', 'III', 'CSIS'),
(18, 'CS380', 'Software Engineering', 'Anel Tanovic', 'III', 'CSIS'),
(19, 'Lang321E', 'English Language - CSIS', 'Gina Landor', 'III', 'CSIS'),
(20, 'Lang321G', 'German Language Edin Konjhodzic - CSIS', 'Edin Konjhodzic ', 'III', 'CSIS'),
(21, 'Lang321G', 'German Language Nejra Ljubovic - CSIS', 'Nejra Ljubovic ', 'III', 'CSIS'),
(22, 'Lang321G', 'German Language Dzana Zahirovic - CSIS', 'Dzana Zahirovic', 'III', 'CSIS'),
(23, 'EE110', 'Fundamentals of Electrical Engineering', 'Elvisa Becirevic', 'III', 'CSIS'),
(24, 'IS490', 'Selected Topics in Information Systems', 'Jasmina Ahmetbasic', 'IV', 'CSIS'),
(25, 'CS360', 'Advanced Algorithm Design', 'Dzejla Medjedovic', 'IV', 'CSIS'),
(26, 'CS390', 'Computer Graphics', 'Vedad Hulusic', 'IV', 'CSIS'),
(27, 'IS375', 'Human Computer Interaction', 'Kemal Taljanovic', 'IV', 'CSIS'),
(28, 'Lang421', 'German Language Edin Konjhodzic - CSIS', 'Edin Konjhodzic ', 'IV', 'CSIS'),
(29, 'Lang421', 'German Language Nejra Ljubovic - CSIS', 'Nejra Ljubovic ', 'IV', 'CSIS'),
(30, 'Lang421', 'German Language Dzana Zahirovic - CSIS', 'Dzana Zahirovic', 'IV', 'CSIS'),
(31, 'EE210', 'EE: Digital Signal Processing I', 'Amila Akagic', 'IV', 'CSIS'),
(32, 'E460', 'Power Systems', 'Ema Halilovic', 'IV', 'CSIS'),
(33, 'EC110', 'Introduction to Microeconomics', 'Adisa Omerbegovic Arapovic', 'I', 'ECON'),
(34, 'EC160', 'Management', 'Rimac Maja', 'I', 'ECON'),
(35, 'EC150', 'Mathematical Analysis for Economics', 'Demirovic A.', 'I', 'ECON'),
(36, 'EC145', 'Comparative Economic Systems', 'Ridjic Goran', 'I', 'ECON'),
(37, 'Lang121E', 'English Language - ECON', 'Anida Hadzic Sabina Gadzo-Mutapcic', 'I', 'ECON'),
(38, 'Lang121G', 'German Language Edin Konjhodzic - ECON', 'Edin Konjhodzic ', 'I', 'ECON'),
(39, 'Lang121G', 'German Language Nejra Ljubovic - ECON', 'Nejra Ljubovic ', 'I', 'ECON'),
(40, 'Lang121G', 'German Language Dzana Zahirovic - ECON', 'Dzana Zahirovic', 'I', 'ECON'),
(41, 'EC240', 'Intermediate Microeconomics', 'Ridjic Goran', 'II', 'ECON'),
(42, 'EC281', 'Introduction to Econometrics (Program Specification UB)', 'Demirovic A.', 'II', 'ECON'),
(43, 'EC330', 'Introduction to Accounting', 'Memic Deni', 'II', 'ECON'),
(44, 'Lang221E', 'Language (English) - ECON', 'Sabina Gadzo-Mutapcic', 'II', 'ECON'),
(45, 'Lang221G', 'German Language Edin Konjhodzic - ECON', 'Edin Konjhodzic ', 'II', 'ECON'),
(46, 'Lang221G', 'German Language Nejra Ljubovic - ECON', 'Nejra Ljubovic ', 'II', 'ECON'),
(47, 'Lang221G', 'German Language Dzana Zahirovic - ECON', 'Dzana Zahirovic', 'II', 'ECON'),
(48, 'EC390', 'Economics and Politics of Development', 'Zivkovic Matej', 'III', 'ECON'),
(49, 'EC340', 'Labor Economics', 'Mirascic Goran', 'III', 'ECON'),
(50, 'EC220', 'International Economics', 'Latic Tanja', 'III', 'ECON'),
(51, 'Lang321E', 'Language (English) - ECON', 'Gina Landor', 'III', 'ECON'),
(52, 'Lang321G', 'German Language Edin Konjhodzic - ECON', 'Edin Konjhodzic ', 'III', 'ECON'),
(53, 'Lang321G', 'German Language Nejra Ljubovic - ECON', 'Nejra Ljubovic ', 'III', 'ECON'),
(54, 'Lang321G', 'German Language Dzana Zahirovic - ECON', 'Dzana Zahirovic', 'III', 'ECON'),
(55, 'EC350', 'Entrepreneurship - ECON', 'Ahmetbasic Jasmina', 'III', 'ECON'),
(56, 'EC320', 'Financial Markets (Program Specification UB)', 'Hadziomeragic Amir', 'III', 'ECON'),
(57, 'EC410', 'Natural Resource and Environmental Economics', 'Adisa Omerbegovic Arapovic', 'IV', 'ECON'),
(58, 'EC490', 'Thesis Writing', '', 'IV', 'ECON'),
(59, 'EC455', 'History of Economic Thought', 'Ridjic Goran', 'IV', 'ECON'),
(60, 'Lang421', 'German Language Edin Konjhodzic - ECON', 'Edin Konjhodzic ', 'IV', 'ECON'),
(61, 'Lang421', 'German Language Nejra Ljubovic - ECON', 'Nejra Ljubovic ', 'IV', 'ECON'),
(62, 'Lang421', 'German Language Dzana Zahirovic - ECON', 'Dzana Zahirovic', 'IV', 'ECON'),
(63, 'EC415', 'Enterprise Resource Planing', 'Celjo Amer', 'IV', 'ECON'),
(64, 'EC350', 'Entrepreneurship - ECON', 'Ahmetbasic Jasmina', 'IV', 'ECON'),
(65, 'EC470', 'Financial Instruments', ' Music Hamza  ', 'IV', 'ECON'),
(66, 'EC569', 'Globalization Trade and Capital Flows', 'Goran Mirascic', '', 'ECON'),
(67, 'EC566', 'Economic Development and Transition', 'Matej Zivkovic', '', 'ECON'),
(68, 'MBA510', 'Operation Management', 'Amer Celjo', '', 'ECON'),
(69, 'PS180', 'Introduction to Politics', 'Schwarz-Schilling/Sabina Cudic ', 'I', 'PSIR'),
(70, 'PS120', 'Modern Political History', 'Maja Savic-Bojanic ', 'I', 'PSIR'),
(71, 'PS190', 'Academic Writing for Social Science', 'Anida Hadzic ', 'I', 'PSIR'),
(72, 'Lang121E', 'English Language - PSIR', 'Anida Hadzic Sabina Gadzo-Mutapcic', 'I', 'PSIR'),
(73, 'Lang121G', 'German Language Edin Konjhodzic - PSIR', 'Edin Konjhodzic ', 'I', 'PSIR'),
(74, 'Lang121G', 'German Language Nejra Ljubovic - PSIR', 'Nejra Ljubovic ', 'I', 'PSIR'),
(75, 'Lang121G', 'German Language Dzana Zahirovic - PSIR', 'Dzana Zahirovic', 'I', 'PSIR'),
(76, 'PS210', 'State and Government', 'Damir Arnaut ', 'II', 'PSIR'),
(77, 'PS270', 'Media and Politics', 'Zeljka Lekic ', 'II', 'PSIR'),
(78, 'PS240', 'Introduction to Political Thought', 'Maja Pulic ', 'II', 'PSIR'),
(79, 'PS230', 'Comparative Politics', 'Matilde Fruncillo ', 'II', 'PSIR'),
(80, 'Lang221E', 'Language (English) - PSIR', 'Sabina Gadzo-Mutapcic', 'II', 'PSIR'),
(81, 'Lang221G', 'German Language Edin Konjhodzic - PSIR', 'Edin Konjhodzic ', 'II', 'PSIR'),
(82, 'Lang221G', 'German Language Nejra Ljubovic - PSIR', 'Nejra Ljubovic ', 'II', 'PSIR'),
(83, 'Lang221G', 'German Language Dzana Zahirovic - PSIR', 'Dzana Zahirovic', 'II', 'PSIR'),
(84, 'PS310', 'International Law and HR', 'Damir Arnaut ', 'III', 'PSIR'),
(85, 'PS320', 'Political Psychology', 'Enisa Mesic', 'III', 'PSIR'),
(86, 'PS330', 'Basics of EU Politics in Int. Context*', 'Adnan Huskić ', 'III', 'PSIR'),
(87, 'Lang321E', 'Language (English) - PSIR', 'Gina Landor', 'III', 'PSIR'),
(88, 'Lang321G', 'German Language Edin Konjhodzic - PSIR', 'Edin Konjhodzic ', 'III', 'PSIR'),
(89, 'Lang321G', 'German Language Nejra Ljubovic - PSIR', 'Nejra Ljubovic ', 'III', 'PSIR'),
(90, 'Lang321G', 'German Language Dzana Zahirovic - PSIR', 'Dzana Zahirovic', 'III', 'PSIR'),
(91, 'PS471', 'Diplomacy: Theory and Practice', '  Matilde Fruncillo   ', 'III', 'PSIR'),
(92, 'PS420', 'Thesis/research design', 'Maja Pulic', 'IV', 'PSIR'),
(93, 'PS432', 'Multiethnic States', 'Vedrana Durakovic ', 'IV', 'PSIR'),
(94, 'Lang421', 'German Language Edin Konjhodzic - PSIR', 'Edin Konjhodzic ', 'IV', 'PSIR'),
(95, 'Lang421', 'German Language Nejra Ljubovic - PSIR', 'Nejra Ljubovic ', 'IV', 'PSIR'),
(96, 'Lang421', 'German Language Dzana Zahirovic - PSIR', 'Dzana Zahirovic', 'IV', 'PSIR'),
(97, 'PD501', 'Diplomacy in Theory and Practice', 'Matilde Fruncillo', 'V', 'PSIR'),
(98, 'PD503', 'Basics of EU Politics of Intenrational Context', 'Adnan Huskic', 'V', 'PSIR'),
(99, 'PD507', 'Processes of International Negotiations', 'Adnan Huskic', 'V', 'PSIR'),
(100, 'PD508', 'Law and Politics of International Conflict Management', 'Adnan Huskic', 'V', 'PSIR'),
(101, 'CAR510', 'Conflict Culture and Democracy', 'Valery Perry', 'V', 'PSIR'),
(102, 'CAR520', 'Introduction to Conflict Analyses', 'Valery Perry', 'V', 'PSIR'),
(103, 'CAR620', 'Genocide in Theory and International Law', 'Francesco de Sanctis', 'V', 'PSIR'),
(104, 'CAR640', 'Media Peace and Conflict', 'Zeljka Lekic', 'V', 'PSIR'),
(105, 'SMS 111', 'Introduction to Medicine', 'Dr Haris Vranić                                                             ', 'I', 'Medicine'),
(106, 'SMS 112', 'Medical Biology', 'Dr Aida Saračević,   Dr Mirza Ibrišimović (lab exercises and seminars) Monia Avdić-teaching assistant  ', 'I', 'Medicine'),
(107, 'SMS 113', 'Medical Physics and Biophysics', 'Dr Adnan Beganović, Dr Lamija Tanović   Dinka Delić-Bojadžija -teaching assistant   ', 'I', 'Medicine'),
(108, 'SMS 114', 'Anatomy I', 'Dr Sandra Vegar,    Dr Faris  Fočo ,  Dr Faruk Dilberović   Igor Kulašin,  Edin Prelević –teaching assistants  ', 'I', 'Medicine'),
(109, 'SMS125', 'Lating Language', 'Snježana Reberšak-Perić', 'I', 'Medicine'),
(110, 'Elang301', 'English Sentence Structure 1', 'Nadira Aljovic Sabina Gadzo-Mutapcic', 'III', 'English'),
(111, 'Elit310', 'Modern American Literature', 'Anida Hadzic', 'III', 'English'),
(112, 'Tefl301', 'TEFL Methods', 'Larisa Kasumagic-Kafedžić', 'III', 'English'),
(113, 'Lang321G', 'German Language Edin Konjhodzic - English', 'Edin Konjhodzic ', 'III', 'English'),
(114, 'EC415', 'Entreprise Resource Planning', ' Celjo Amer', 'IV', 'ECON'),
(115, 'PTest01', 'Subject Test', 'Test Prof', 'IV', 'CSIS');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
