tinyMCE.init({
        // General options
        mode : "exact",
        elements : "tinymce",
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        width : "695",
        height : "400",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect,|,sub,sup,|,charmap,emotions,iespell,media,|,fullscreen",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,preview,|,forecolor,backcolor,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,ltr,rtl,",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
//        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
//        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
//        theme_advanced_buttons3 : "styleselect,formatselect,fontselect,fontsizeselect,|,sub,sup,|,charmap,emotions,iespell,media,|,fullscreen",
//        theme_advanced_buttons4 : "preview,|,forecolor,backcolor,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,ltr,rtl",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // KCFinder
        // vea note - callback function defined in "set-file-finder.js""
        file_browser_callback: 'openKCFinder',

        // Example content CSS (should be your site CSS)
        // content_css : "/css/extra-styles.css",
        // content_css : "/css/style.css",
        content_css : "/css/extra-styles.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "/scripts/tinymce/tiny_mce_template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "",
        media_external_list_url : "js/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});