/* Author: vea */
$(document).ready(function(){


    // vea - file select/upload polje
    $('#btnAdd').click(function() {
            var num = $('.clonedInput').length;
            var newNum = new Number(num + 1);
            var newElem = $('#prilozi-div' + num).clone().attr('id', 'prilozi-div' + newNum);
            newElem.children('input:first').attr('id', 'p' + newNum).val("");
            newElem.children('button:first').attr('name','p'+newNum);
            $('#prilozi-div' + num).after(newElem);
            $('#btnDel').removeAttr('disabled');
            $('#counter').val(newNum);
            if (newNum == 10) $('#btnAdd').attr('disabled','disabled');
            console.log(num);
    });

    $('#btnDel').click(function() {
            var num = $('.clonedInput').length;
            $('#prilozi-div' + num).remove();
            $('#counter').val( num-1 );
            $('#btnAdd').removeAttr('disabled');
            if (num-1 == 1)
                    $('#btnDel').attr('disabled','disabled');
        });

    var na = $('.clonedInput').length;
    if (na > 1) $('#btnDel').attr('disabled',''); else $('#btnDel').attr('disabled','disabled'); 
    $('#counter').val(na);
    $('#btnValDel').click(function () { $('[id^="p"]:last').val(""); });

    $('.reset-prev').click(function(){
           $(this).prev().val("");
       });


    $('.decrease').click(function(){
        if ($(this).prev('[type=text]').val() >= 1)
           $(this).prev('[type=text]').val( Number($(this).prev('[type=text]').val()) - 1 );
       });
    $('.increase').click(function(){
           $(this).prev().prev('[type=text]').val( Number($(this).prev().prev('[type=text]').val()) + 1 );
       });

    //       Autocomplete
    $(".autocomplete").each(function(){
        $(this).autocomplete({
                source: "http://www.spaja-lica.com/admini/ajax/" + $(this).attr('id'),
                minLength: 1
        });
    });

    // Accordion
    $("#accordion").accordion({
        header: "h3",
        autoHeight: false,
        collapsible: true,
        active: false
    });

    $("#settings-accordion").accordion({
        header: "h3",
        autoHeight: false,
        collapsible: true,
        icons: { "header": "ui-icon-plus", "headerSelected": "ui-icon-minus" },
        active: false
    });

    // Radio buttons
    $( "#radio" ).buttonset();

    // Tabs
    $('#tabs').tabs();

    // Dialog
    $('#dialog').dialog({
    autoOpen: false,
    width: 600,
    buttons: {
    "Ok": function() {
    $(this).dialog("close");
    },
    "Cancel": function() {
    $(this).dialog("close");
    }
    }
    });

    // Dialog Link
    $('#dialog_link').click(function(){
                $('#dialog').dialog('open');
                return false;
    });

    // Datepicker
    $('.datepicker').each(function(){
        $(this).datetimepicker({
                showWeek: true,
                showSecond: true,
                firstDay: 1,
                dateFormat: "yy-mm-dd",
                timeFormat: "hh:mm:ss"
        });
    });

    // Slider
    $('#slider').slider({
    range: true,
    values: [17, 67]
    });

    // Progressbar
    $("#progressbar").progressbar({
    value: 20
    });

    $('.confirm').click(function () {

        if ($("#confirm-dialog").length != 0) {
            $("#confirm-dialog").remove();
        }

        $("<div></div>").attr('id','confirm-dialog').appendTo('body');

        $("#confirm-dialog").append("<p>Dali ste sigurni da želi te izvršiti akciju: <strong>\""+$(this).attr('action')+"\"</strong> za: <br/>"+$(this).attr('message')+"</p>");

        var link = $(this).attr('url');

        $("#confirm-dialog").dialog({
                    title: 'Confirm action: ' + $(this).attr('action'),
                    buttons: {
                        OK : function () { location.href = link; }, // vea add - iz nekog razloga u $(this).attr('action') vrati parametar iz pozvanog objekta ali $(this).attr('uri') kada je unutar bloka "buttons" ne vrati ofgovarajucu vrijednost"
                        Cancel : function () {$(this).dialog("close"); }
                    },
                    width: 400
                }
            );
    });


    // tags magic
    $('#tags').tagsInput({
       'autocomplete_url': 'http://www.spaja-lica.com/admini/ajax/tags?tags=',
       'autocomplete':{width:'200px',autoFill:true},
        'width':'auto',
        'height':'15px',
       'interactive':true,
       'defaultText':'Dodaj tag',
       'removeWithBackspace' : true,
        'minChars' : 1,
       'placeholderColor' : '#666666',
       'onAddTag': function(){},
       'onRemoveTag':function(){},
       'onChange' : function(){}
    });


    //hover states on the static widgets
    $('#dialog_link, ul#icons li').hover(
    function() {$(this).addClass('ui-state-hover');},
    function() {$(this).removeClass('ui-state-hover');}
    );

    // Hide top button
    $('#top-link').hide();
    $(window).scroll(function() {
    	var scroll_pos = $(window).scrollTop();
    	if (scroll_pos > 100 && scroll_pos < 1000) {
    		$('#top-link').show();
    		var opacity_val = Math.floor(((scroll_pos/1000) * 0.6) * 10) / 10;
    		$('#top-link').css('opacity', opacity_val);
    	} else if(scroll_pos < 100) {
                                $('#top-link').hide();
                        }
        });

    $('#top-link').click(function(){
                $(this).hide();
                $('html, body').animate({scrollTop:0}, 'slow');
        });

     });

    $("a#help-link").click(function() {
            var num = $(this).attr('rel');
            $( "#dialog"+num ).dialog( "open" );
            return false;
    });

    // vea - help dialog
    $('[id^=dialog]').dialog({
            autoOpen: false,
            position: [50,100],
            closeOnEscape: true
    });

/* Top scroll handling */


/* Add innerfade to gallery */
//$('#gallery ul').innerfade({
//	speed: 1000,
//	timeout: 5000,
//	type: 'sequence',
//	containerheight: '200px'
//});

$('div.round-segment').corner("round 7px");
$('.round-segment').corner("round 4px");


// slider add dell ....

