// // Open KCFinder function!
// vea note - backcalled from "set-text-editor.js"

function openKCFinder(field_name, url, type, win) {
    tinyMCE.activeEditor.windowManager.open({
        file: '/scripts/kcfinder/browse.php?opener=tinymce&type=upload',
        title: 'File Browser',
        width: 800,
        height: 700,
        resizable: "yes",
        inline: true,
        close_previous: "no",
        popup_css: false
    }, {
        window: win,
        input: field_name
    });
   // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win); // debug/testing
    return false;
}

/*********************************** openKCFinderPopUp ***************************************/

function openKCFinderPopUp(field,type,dir) {
    window.KCFinder = {
        callBack: function(url) {
            field.value = url;
            window.KCFinder = null;
        }
    };
    window.open('/scripts/kcfinder/browse.php?type='+type+'&dir='+dir, 'kcfinder_textbox',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600'
    );
}

/*********************************** openKCFinderIframe ***************************************/

function openKCFinderIframe(field,type,dir) {
    var div = document.getElementById('kcfinder_iframe_div');
    if (div.style.display == "block") {
        div.style.display = 'none';
        div.innerHTML = '';
        return;
    }
    window.KCFinder = {
        callBack: function(url) {
            window.KCFinder = null;
            field.value = url;
            div.style.display = 'none';
            div.innerHTML = '';
        }
    };
    div.innerHTML = '<iframe name="kcfinder_iframe" src="/scripts/kcfinder/browse.php?type='+type+'&dir='+dir+'" ' +
        'frameborder="0" width="100%" height="100%" marginwidth="0" marginheight="0" scrolling="no" />';
    div.style.display = 'block';
    // vea add - TODO : treba da se otvori img src ispod polja i da se eventualno otvore opcije za resize i slicno
}


/*********************************** openKCFinderMultipleFiles ***************************************/

function openKCFinderMultipleFiles(textarea,type,dir) {
    window.KCFinder = {
        callBackMultiple: function(files) {
            window.KCFinder = null;
            textarea.value = "";
            for (var i = 0; i < files.length; i++)
                textarea.value += files[i] + "\n";
        }
    };
    window.open('/scripts/kcfinder/browse.php?type='+type+'&dir='+dir,
        'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
    );
}